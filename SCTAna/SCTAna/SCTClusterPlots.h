#ifndef SCTClusterPlots_h
#define SCTClusterPlots_h

#include "SCTAna/SCTBase.h"
#include "SCTAna/SCTModule.h"


class SCTClusterPlots : public SCTBase {
 public:
  SCTClusterPlots();
  virtual ~SCTClusterPlots(){};

  virtual void bookHists();
  virtual TList* getHists();
  virtual void execute(xAOD::TEvent*);
  virtual void finalize();
 private:
  TList* m_histList;

  TH1F* m_timeBin; //!
  TH1F* m_clusterSize; //!
  TH1F* m_clusterLocalX; //!
  TH2F* m_sctClusterGlobalRZ; //!
  TH2F* m_sctClusterGlobalXY; //!
  TH2F* m_sctClusterMapBarrel[8]; //!
  TH2F* m_sctClusterMapECA[18]; //!
  TH2F* m_sctClusterMapECC[18]; //!
  TH2I* m_sctClusterHashChip; //!
  bool m_doTimeBin;
  
  std::map< TString, SCTModule * > m_sctMapping;

  ClassDef(SCTClusterPlots, 1);

};


#endif
