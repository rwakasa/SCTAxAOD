#ifndef SCTTrackPlots_h
#define SCTTrackPlots_h

#include "SCTAna/SCTBase.h"

namespace InDet {
  class InDetTrackSelectionTool;
}

class GoodRunsListSelectionTool;
//#include "GoodRunsLists/GoodRunsListSelectionTool.h"  
   
class SCTTrackPlots : public SCTBase {
 public:
  SCTTrackPlots();
  virtual ~SCTTrackPlots(){};
  
  virtual void bookHists();
  virtual TList* getHists();
  virtual void execute(xAOD::TEvent*);
  virtual void finalize();
  
  InDet::InDetTrackSelectionTool *m_selectionToolLoose; //!
  InDet::InDetTrackSelectionTool *m_selectionToolLoosePrimary; //!
  InDet::InDetTrackSelectionTool *m_selectionToolTightPrimary; //!
  GoodRunsListSelectionTool *m_grl; //!
 private:
  TList* m_histList;
  
  TH1D* m_nTracks; //!
  TH1D* m_nLooseTracks; //!
  TH1D* m_nLoosePrimaryTracks; //!
  TH1D* m_nTightPrimaryTracks; //!

  TH1D* m_nEvents_LB; //!
  TH1D* m_nTracks_LB; //!
  TH1D* m_nLooseTracks_LB; //!
  TH1D* m_nLoosePrimaryTracks_LB; //!
  TH1D* m_nTightPrimaryTracks_LB; //!
  TH1D* m_nTracksBarrel_LB; //!
  TH1D* m_nLooseTracksBarrel_LB; //!
  TH1D* m_nLooseTracksBarrel1GeV_LB; //!
  TH1D* m_nLoosePrimaryTracksBarrel_LB; //!
  TH1D* m_nTightPrimaryTracksBarrel_LB; //!
  TH2D* m_numberOfSCTHitsBarrel_LB; //!
  TH2D* m_numberOfSCTHolesBarrel_LB; //!

  TH2D* m_SiWidthBarrel3_LB[6]; //!

  TH2D* m_pt_LB; //!

  TH2D* m_eta_phi; //!
  TH2D* m_eta_phi_numberOfSCTHits; //!
  TH2D* m_eta_phi_numberOfSCTOutliers; //!
  TH2D* m_eta_phi_numberOfSCTHoles; //!
  TH2D* m_eta_phi_numberOfSCTDoubleHoles; //!
  TH2D* m_eta_phi_numberOfSCTSharedHits; //!
  TH2D* m_eta_phi_numberOfSCTDeadSensors; //!
  TH2D* m_eta_phi_numberOfSCTSpoiltHits; //!

  TH2D* m_numberOfSCTHitsActualInteractionsN; //!
  TH2D* m_numberOfSCTHitsActualInteractionsL; //!
  TH2D* m_numberOfSCTHitsActualInteractionsLP; //!
  TH2D* m_numberOfSCTHitsActualInteractionsTP; //!

  TH2D* m_numberOfSCTHitsLBN; //!
  TH2D* m_numberOfSCTHitsLBL; //!
  TH2D* m_numberOfSCTHitsLBLP; //!
  TH2D* m_numberOfSCTHitsLBTP; //!

  TH1D* m_numberOfSCTHits; //!
  TH1D* m_numberOfSCTOutliers; //!
  TH1D* m_numberOfSCTHoles; //!
  TH1D* m_numberOfSCTDoubleHoles; //!
  TH1D* m_numberOfSCTSharedHits; //!
  TH1D* m_numberOfSCTDeadSensors; //!
  TH1D* m_numberOfSCTSpoiltHits; //!
     
  TH1D* m_prob; //!

  TH1D* m_RDOs; //!
  TH1D* m_Clusters; //!
     
  ClassDef(SCTTrackPlots, 1);
};
  
#endif // SCTTrackPlots_h
     

