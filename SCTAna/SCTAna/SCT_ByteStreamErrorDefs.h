#include <vector>
#include <iostream>
#include <TString.h>

namespace SCT_ByteStreamErrors {
  enum errorTypes {
    ByteStreamParseError,
    TimeOutError,
    BCIDError,
    LVL1IDError,
    PreambleError,
    FormatterError,
    TrailerError,
    TrailerOverflowError,
    HeaderTrailerLimitError,
    ABCDError,
    RawError,
    MaskedLink,
    RODClockError,
    TruncatedROD,
    ROBFragmentError,
    MissingLinkHeaderError,
    MaskedROD,
    ABCDError_Chip0,
    ABCDError_Chip1,
    ABCDError_Chip2,
    ABCDError_Chip3,
    ABCDError_Chip4,
    ABCDError_Chip5,
    ABCDError_Error1,
    ABCDError_Error2,
    ABCDError_Error4,
    TempMaskedChip0,
    TempMaskedChip1,
    TempMaskedChip2,
    TempMaskedChip3,
    TempMaskedChip4,
    TempMaskedChip5,
    ABCDError_Error7,
    ABCDError_Invalid,
    NUM_ERROR_TYPES  // always have this one last, so we can use it as a loop index
  };


}
