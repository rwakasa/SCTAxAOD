#ifndef SCTOccupancyPlots_h
#define SCTOccupancyPlots_h

#include "SCTAna/SCTBase.h"
#include "SCTAna/SCTModule.h"
/// Trigger stuff 
#include "TrigConfxAOD/xAODConfigTool.h"
#include "TrigDecisionTool/TrigDecisionTool.h"

class SCTOccupancyPlots : public SCTBase {
 public:
  SCTOccupancyPlots();
  virtual ~SCTOccupancyPlots(){};

  virtual void bookHists();
  virtual TList* getHists();
  virtual void execute(xAOD::TEvent*);
  virtual void finalize();
 private:
  TList* m_histList;

  TH1F* m_eventCounter; //!
  TH2F* m_sctOccupancyMapBarrel[8]; //!
  TH2F* m_sctOccupancyMapECA[18]; //!
  TH2F* m_sctOccupancyMapECC[18]; //!
  bool m_doTimeBin;
  bool m_isFirstEvent;
  TProfile* m_occupancyVsLayer; //!
  TProfile* m_occupancyVsMu_barrel0; //!
  TProfile* m_occupancyVsMu_barrel1; //!
  TProfile* m_occupancyVsMu_barrel2; //!
  TProfile* m_occupancyVsMu_barrel3; //!
  // trigger tools member variables
  Trig::TrigDecisionTool *m_trigDecisionTool; //!
  TrigConf::xAODConfigTool *m_trigConfigTool; //!

  //  std::map< TString, SCTModule * > m_sctMapping;
  std::vector<int>* m_numModulesPerLayer;
  
  bool m_doTrigger;
  

  ClassDef(SCTOccupancyPlots, 1);

};


#endif
