#ifndef SCTDataSize_h
#define SCTDataSize_h

#include "SCTAna/SCTBase.h"
#include "SCTAna/SCTModule.h"

class SCTDataSize : public SCTBase {
 public:
  SCTDataSize();
  virtual ~SCTDataSize(){};

  virtual void bookHists();
  virtual TList* getHists();
  virtual void execute(xAOD::TEvent*);
  virtual void finalize();
 private:
  TList* m_histList;

  TH1I *m_h_mu; //!
  TH1I *m_h_ABCD; //! 
  TH2I *m_h_muLink; //!
  TH2I *m_h_muABCD; //!
  TH2I *m_h_muLinkBit; //!
  TH2I *m_h_muLinkExpanded; //!
  TH2I *m_h_muLinkCondensed; //!
  TH2I *m_h_muLinkSuperCondensed; //!

  bool m_isFirstEvent; //!

  bool isDisabledChip(int bec, int layer, int eta_module, int phi_module, int side, int chip);
  int getChip(int bec, int layer, int eta_module, int side, int strip);

  ClassDef(SCTDataSize, 1);

};


#endif
