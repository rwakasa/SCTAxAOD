/********************************************************
Name:        SCTModule

Author:      Will Fawcett
Created:     July 2015

Description: Custom class for an SCT Module
********************************************************/



#ifndef SCTMODULE_H
#define SCTMODULE_H

#include <map>
#include <vector>
#include <iostream>

class SCTModule {
 public:

     // Default constructor 
     SCTModule(){ 
         m_hash = -999;
         m_serialNumber = -999;
         m_bec = -999;
         m_layer = -999;
         m_phi = -999;
         m_eta = -999;
         m_barrel = -999;
         m_row = -999;
         m_disk = -999;
         m_quadrant = -999;
         m_number = -999;
         m_crate = -999;
         m_slot = -999;
         m_channel = -999;
         m_pscrate = -999;
         m_pschannel = -999;
     }

     // constructor 
     SCTModule( 
             const int in_hash,
             const int in_serialNumber,
             const int in_bec,
             const int in_layer,
             const int in_phi,
             const int in_eta,
             const int in_barrel,
             const int in_row,
             const int in_disk,
             const int in_quadrant,
             const int in_number,
             const int in_crate,
             const int in_slot,
             const int in_channel,
             const int in_pscrate,
             const int in_pschannel){
         m_hash = in_hash;
         m_serialNumber = in_serialNumber;
         m_bec = in_bec;
         m_layer = in_layer;
         m_phi = in_phi;
         m_eta = in_eta;
         m_barrel = in_barrel;
         m_row = in_row;
         m_disk = in_disk;
         m_quadrant = in_quadrant;
         m_number = in_number;
         m_crate = in_crate;
         m_slot = in_slot;
         m_channel = in_channel;
         m_pscrate = in_pscrate;
         m_pschannel = in_pschannel;
     }

     // Destructor 
     ~SCTModule() {}
    
    // Accessor function
    int hash() {return m_hash;}
    int serialNumber() {return m_serialNumber;}
    int bec() {return m_bec;}
    int layer() {return m_layer;}
    int phi() {return m_phi;}
    int eta() {return m_eta;}
    int barrel() {return m_barrel;}
    int row() {return m_row;}
    int disk() {return m_disk;}
    int quadrant() {return m_quadrant;}
    int number() {return m_number;}
    int crate() {return m_crate;}
    int slot() {return m_slot;}
    int channel() {return m_channel;}
    int pscrate() {return m_pscrate;}
    int pschannel() {return m_pschannel;}

    void sethash(int in) {m_hash=in;}
    void setserialNumber(int in) {m_serialNumber=in;}
    void setbec(int in) {m_bec=in;}
    void setlayer(int in) {m_layer=in;}
    void setphi(int in) {m_phi=in;}
    void seteta(int in) {m_eta=in;}
    void setbarrel(int in) {m_barrel=in;}
    void setrow(int in) {m_row=in;}
    void setdisk(int in) {m_disk=in;}
    void setquadrant(int in) {m_quadrant=in;}
    void setnumber(int in) {m_number=in;}
    void setcrate(int in) {m_crate=in;}
    void setslot(int in) {m_slot=in;}
    void setchannel(int in) {m_channel=in;}
    void setpscrate(int in) {m_pscrate=in;}
    void setpschannel(int in) {m_pschannel=in;}


    // Other member functions 
    void printModule(){
        std::cout << "hash: "         << m_hash         << std::endl;
        std::cout << "serialNumber: " << m_serialNumber << std::endl;
        std::cout << "bec: "          << m_bec          << std::endl;
        std::cout << "layer: "        << m_layer        << std::endl;
        std::cout << "phi: "          << m_phi          << std::endl;
        std::cout << "eta: "          << m_eta          << std::endl;
        std::cout << "barrel: "       << m_barrel       << std::endl;
        std::cout << "row: "          << m_row          << std::endl;
        std::cout << "disk: "         << m_disk         << std::endl;
        std::cout << "quadrant: "     << m_quadrant     << std::endl;
        std::cout << "number: "       << m_number       << std::endl;
        std::cout << "crate: "        << m_crate        << std::endl;
        std::cout << "slot: "         << m_slot         << std::endl;
        std::cout << "channel: "      << m_channel      << std::endl;
        std::cout << "pscrate: "      << m_pscrate      << std::endl;
        std::cout << "pschannel: "    << m_pschannel    << std::endl;
    }


 private:
  int m_hash;
  int m_serialNumber;
  int m_bec;
  int m_layer;
  int m_phi;
  int m_eta;
  int m_barrel;
  int m_row;
  int m_disk;
  int m_quadrant;
  int m_number;
  int m_crate;
  int m_slot;
  int m_channel;
  int m_pscrate;
  int m_pschannel;

};

#endif // SCTMODULE_H
