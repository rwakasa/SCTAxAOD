#ifndef SCTByteStreamErrorPlots_h
#define SCTByteStreamErrorPlots_h

#include "SCTAna/SCTBase.h"
#include "SCTAna/SCT_SimpleCabling.h"


class SCTByteStreamErrorPlots : public SCTBase {
 public:
  SCTByteStreamErrorPlots();
  virtual ~SCTByteStreamErrorPlots(){};

  virtual void bookHists();
  virtual TList* getHists();
  virtual void execute(xAOD::TEvent*);
  virtual void finalize();
 private:
  TList* m_histList;
  SCT_SimpleCabling* m_cabling;

  //// declare your histograms here, and dont forget a //!  at the end of each line..
  TH2F* m_sctBSErrorMapBarrel[152]; //!
  TH2F* m_sctBSErrorMapECA[342]; //!
  TH2F* m_sctBSErrorMapECC[342]; //!
  TH1F* m_sctBSErrType; //!
  
  int m_eventCounter;
  std::vector<TString>* m_errorNames;

  ClassDef(SCTByteStreamErrorPlots, 1);

};


#endif
