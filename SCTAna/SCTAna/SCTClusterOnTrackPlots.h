#ifndef SCTClusterOnTrackPlots_h
#define SCTClusterOnTrackPlots_h

#include "SCTAna/SCTBase.h"
#include <TProfile.h>


class SCTClusterOnTrackPlots : public SCTBase {
 public:
  SCTClusterOnTrackPlots();
  virtual ~SCTClusterOnTrackPlots(){};

  virtual void bookHists();
  virtual TList* getHists();
  virtual void execute(xAOD::TEvent*);
  virtual void finalize();
 private:
  TList* m_histList;
  TH1F* m_msosDetType; //!
   
  TH1F* m_timeBinTrack; //!
  TH1F* m_nSCTHitsOnTrack; //!
  TH2F* m_nPixVsSCTHitsOnTrack; //!
  TH2F* m_sctClusterGlobalRZTrack; //!
  TH2F* m_sctClusterGlobalXYTrack; //!
  TH2F* m_sctClusterOnTrackMapBarrel[8]; //!
  TH2F* m_sctClusterOnTrackMapECA[18]; //!
  TH2F* m_sctClusterOnTrackMapECC[18]; //!
  TH1F* m_sctClusLocalPhi; //!
  TH1F* m_sctClusLocalTheta; //!
  TH1F* m_sctClusOnTrackLocalX; //!
  TH1F* m_sctClusUnbiasedResidualX; //!
  TH1F* m_trackPT; //!
  TH1F* m_trackEta; //!
  TH1F* m_trackPhi; //!
  TH1F* m_trackD0; //!
  TH1F* m_trackPT_withSCT; //!
  TH1F* m_trackEta_withSCT; //!
  TH1F* m_trackPhi_withSCT; //!
  TH1F* m_trackD0_withSCT; //!
  TH1F* m_sctClusOnTrackSize; //
  TH1F* m_sctClusOnTrackTimeBin; //!
  
  TProfile* m_sctClusSizeVsLocalPhi; //!
  TProfile* m_nSCTHitsOnTrackVsEta; //!
  TProfile* m_nSCTHolesOnTrackVsEta; //!
  bool m_doTimeBin;
  ClassDef(SCTClusterOnTrackPlots, 1);

};


#endif
