#ifndef SCTAlgTemplate_h
#define SCTAlgTemplate_h

#include "SCTAna/SCTBase.h"



class SCTAlgTemplate : public SCTBase {
 public:
  SCTAlgTemplate();
  virtual ~SCTAlgTemplate(){};

  virtual void bookHists();
  virtual TList* getHists();
  virtual void execute(xAOD::TEvent*);
  virtual void finalize();
 private:
  TList* m_histList;
  //// declare your histograms here, and dont forget a //!  at the end of each line..

  ClassDef(SCTAlgTemplate, 1);

};


#endif
