#ifndef SCT_SimpleCabling_h
#define SCT_SimpleCabling_h

#include <map>
#include <vector>
#include <iostream>
#include <fstream>

class SCT_SimpleCabling {
 public:
  SCT_SimpleCabling();
  virtual ~SCT_SimpleCabling();
  virtual void readCablingFromText();

 private:
  std::map<int,int> m_hashIdToRobId;

};

#endif
