#ifndef SCTEnergyLossStudyPlots_h
#define SCTEnergyLossStudyPlots_h

#include "SCTAna/SCTBase.h"
#include <TProfile.h>


class SCTEnergyLossStudyPlots : public SCTBase {
 public:
  SCTEnergyLossStudyPlots();
  virtual ~SCTEnergyLossStudyPlots(){};

  virtual void bookHists();
  virtual TList* getHists();
  virtual void execute(xAOD::TEvent*);
  virtual void finalize();
 private:
  TList* m_histList;

  TH1F* m_timeBinTrack;//!
  TH1F* m_timeBinHist;//!
  TProfile* m_timeBinProfile; //!  
  TH1F* m_sctClusLocalPhi_before; //!
  TH1F* m_sctClusLocalTheta_before; //!
  TH1F* m_sctClusLocalPhi_after; //!
  TH1F* m_sctClusLocalTheta_after; //!
  TH1F* m_dEdxSCT; //!
  TH2F* m_dEdxVsqp; //!
  TH1F* m_hitsOntrack;//!

  ClassDef(SCTEnergyLossStudyPlots, 1);

};


#endif