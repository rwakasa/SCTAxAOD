
namespace TrackState {
  enum MeasurementType {
    unidentified = 0,
    Pixel      = 1,
    SCT        = 2,
    TRT        = 3,
    MDT        = 4,
    CSC        = 5,
    RPC        = 6,
    TGC        = 7,
    Pseudo     = 8,
    Vertex     = 9,
    Segment    = 10,
    SpacePoint = 11,
    LArCal     = 12,
    TileCal    = 13,
    STGC       = 14,
    MM         = 15,
    NumberOfMeasurementTypes=16	
  };	
}


// Copy of enum TrackStateOnSurfaceType from Athena: Tracking/TrkEvent/TrkTrack/TrkTrack/TrackStateOnSurface.h

namespace TrackStateOnSurface {
  enum TrackStateOnSurfaceType {
    /** This is a measurement, and will at least contain a Trk::MeasurementBase*/
    Measurement=0,
    /** This represents inert material, and so will contain MaterialEffectsBase */
    InertMaterial=1,
    /** This represents a brem point on the track,
     * and so will contain TrackParameters and MaterialEffectsBase */
    BremPoint=2,
    /** This represents a scattering point on the track,
     * and so will contain TrackParameters and MaterialEffectsBase */
    Scatterer=3,
    /** This represents a perigee, and so will contain a Perigee
     * object only*/
    Perigee=4,
    /** This TSoS contains an outlier, that is, it contains a
     * MeasurementBase/RIO_OnTrack which was not used in the track
     * fit*/
    Outlier=5,
    /** A hole on the track - this is defined in the following way.
     * A hole is a missing measurement BETWEEN the first and last
     * actual measurements. i.e. if your track starts in the SCT,
     * you should not consider a missing b-layer hit as a hole.*/
    Hole=6,
    /** For some reason this does not fall into any of the other categories
     * PLEASE DO NOT USE THIS - DEPRECATED!*/
    Unknown=7,
    /** This TSOS contains a CaloEnergy object*/
    CaloDeposit=8,	
    /**
     * This TSOS contains a Trk::MeasurementBase	
     */
    Parameter=9,	
    /**
     * This TSOS contains a Trk::FitQualityOnSurface	
     */
    FitQuality=10,
    NumberOfTrackStateOnSurfaceTypes=11	
  };
}
