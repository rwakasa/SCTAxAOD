#ifndef SCTHitEfficiency_h
#define SCTHitEfficiency_h

#include "SCTAna/SCTBase.h"
#include <TProfile.h>
#include <TProfile2D.h>

#include "TTree.h"

class SCTHitEfficiency : public SCTBase {
 public:
  SCTHitEfficiency();
  virtual ~SCTHitEfficiency(){};

  virtual void bookHists();
  virtual TList* getHists();
  virtual void execute(xAOD::TEvent*);
  virtual void finalize();
 private:

  TList* m_histList;

  // TTree-related variables
  TTree* m_trackTree;
  TTree* m_eventTree;
  typedef struct{
    // track info
    float p;
    float pT;
    float m;
    float pti;
    float d0;
    float z0;
    float angle;
    float chi2;
    float dof;
    float trkphi;
    float trketa;
    float trktheta;
    int   nhit_SCT;
    int   nhit_Pixel;
    int   nhit_Trt;
    int   nout_SCT;
    int   nout_Pixel;
    int   nout_Trt;
    int   flagB3Unbiased;
    // hit or hole
    std::vector<int>   hit_bec;
    std::vector<int>   hit_layer;
    std::vector<int>   hit_eta;
    std::vector<int>   hit_phi;
    std::vector<int>   hit_side;
    std::vector<float> hit_localx;
    std::vector<float> hit_localy;
    std::vector<float> hit_residualx;
    std::vector<float> hit_residualy;
    std::vector<float> hit_localphi;
    std::vector<float> hit_localtheta;
    std::vector<double>hit_chi2;
    std::vector<float> hit_residual;
    std::vector<float> hit_pullx;
    std::vector<float> hit_pully;
    std::vector<int>   hit_edgeStrip;
    std::vector<int>   hit_clusize;
    std::vector<int>   hole_bec;
    std::vector<int>   hole_layer;
    std::vector<int>   hole_eta;
    std::vector<int>   hole_phi;
    std::vector<int>   hole_side;
    std::vector<float> hole_localx;
    std::vector<float> hole_localy;
    std::vector<float> hole_localphi;
    std::vector<float> hole_localtheta;
    std::vector<double>hole_chi2;
    std::vector<float> hole_residual;
    std::vector<float> hole_residualx;
    std::vector<float> hole_residualy;
    std::vector<float> hole_pullx;
    std::vector<float> hole_pully;


    // module info
    int   bec;
    int   layer;
    int   eta;
    int   phi;
    int   side;
    // event info
    int   run;
    int   lb;
    int   event;
    int   bcid;
    int   ntrack;
    int   ntrack_aftercut;
    // BS errors
    std::vector<int> bsErr_bec;
    std::vector<int> bsErr_layer;
    std::vector<int> bsErr_eta;
    std::vector<int> bsErr_phi;
    std::vector<int> bsErr_side;
    std::vector<int> bsErr_type;
    // misc
    int   track;
    int   nhole;
    int   nhit;
  } Branch_t;

  Branch_t m_branch;

  ClassDef(SCTHitEfficiency, 1);


};


#endif
