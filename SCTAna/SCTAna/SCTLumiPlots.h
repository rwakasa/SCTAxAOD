#ifndef SCTLumiPlots_h
#define SCTLumiPlots_h

#include "SCTAna/SCTBase.h"
#include <TH3.h>
#include <TH2.h>
#include <TH1.h>
#include <TF1.h>
#include <TProfile.h>
#include <TStyle.h>
#include <TCanvas.h>
#include "TGraph.h"



class SCTLumiPlots : public SCTBase {
 public:
  SCTLumiPlots();
  virtual ~SCTLumiPlots(){};

  virtual void bookHists();
  virtual TList* getHists();
  virtual void execute(xAOD::TEvent*);
  virtual void finalize();

 private:





  TList* m_histList;
  
  TH1F*     m_timeBin; //!
  TH1F*     m_timeBin_onPeak; //!
  TH1F*     m_timeBin_PeakLeft; //!
  TH1F*     m_timeBin_PeakRight; //!
  TH1F*     m_timeBin_PeakAway; //!
  TH1F*     m_timeBin_PeakRightRight; //!
  TH1F*     m_timeBin_PeakRightRightRight; //!
  TH1F*     m_clusterSize; //!
  TH1F*     m_clusterSize_onPeak; //!
  TH1F*     m_clusterSize_PeakLeft; //!
  TH1F*     m_clusterSize_PeakRight; //!
  TH1F*     m_clusterSize_PeakRightRight; //!
  TH1F*     m_clusterSize_PeakRightRightRight; //!
  TH1F*     m_clusterSize_PeakAway; //!
  TH2F*     m_BCIDvsTimebin;//!
  TH2F*     m_clusterSizevsTimebin;//!
  TH2F*     m_clusterSizevsBCID;//!
  TH2F*     m_clusterSizevsBCID_barrel_layer[4];//!
  TH1F*     m_clusterLocalX; //!
  TH2F*     m_sctClusterGlobalRZ; //!
  TH2F*     m_sctClusterGlobalXY; //!
  TH2F*     m_sctClusterMapBarrel[8]; //!
  TH2F*     m_sctClusterMapECA[18]; //!
  TH2F*     m_sctClusterMapECC[18]; //!
  TProfile* clus_vs_BCID;
  TProfile* clus_vs_BCID_barrel;
  TProfile* clus_vs_BCID_endcap;
  TProfile* strips_vs_mu;
  TProfile* stripsmu_vs_mu;
  TProfile* strips_vs_BCID;
  TProfile* clus_vs_mu;
  TProfile* clusmu_vs_mu;
  TProfile* tracks_vs_BCID;
  TProfile* selectedTracks_vs_BCID;
  TProfile* m_clus_vs_mu[4];
  TProfile* clus_vs_BCID_barrel_layer[4];
 
  
  

  bool      m_doTimeBin;
  
  ClassDef(SCTLumiPlots, 1);

};


#endif
