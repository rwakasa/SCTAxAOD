/********************************************************
Name:        SCTModule

Author:      Will Fawcett
Created:     July 2015

Description: Read cabling file and return SCT mapping
********************************************************/

#ifndef SCTModuleCoordMap_h
#define SCTModuleCoordMap_h

#include <map>
#include <SCTAna/SCTModule.h>
#include <TString.h>
#include <string>

std::map< TString, SCTModule * > getSCTMapping(std::string);


#endif

