#include <iostream>
#include "SCTAna/SCTAlgTemplate.h"

#include "xAODRootAccess/TStore.h"
#include "xAODCore/ShallowCopy.h"

SCTAlgTemplate::SCTAlgTemplate() 
{
  /// create a TList to hold the histograms
  m_histList = new TList();

}
void SCTAlgTemplate::bookHists() 
{
  /// create histograms and Add them to m_histList 
}

TList* 
SCTAlgTemplate::getHists() 
{
  return m_histList;
}

void 
SCTAlgTemplate::execute(xAOD::TEvent* thisEvent) 
{
  
  //// your code goes here!

   
}

void 
SCTAlgTemplate::finalize() 
{
  
}



