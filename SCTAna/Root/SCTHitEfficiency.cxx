#include <iostream>
#include "SCTAna/SCTHitEfficiency.h"
#include <bitset>
#include <string>

// EDM includes: - if move to header file will not compile?
#include "xAODEventInfo/EventInfo.h"
#include "xAODTruth/TruthParticleAuxContainer.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthVertex.h"
#include "xAODTracking/TrackMeasurementValidationContainer.h"
#include "xAODTracking/TrackStateValidationContainer.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTracking/TrackParticleAuxContainer.h"

#include "xAODRootAccess/TStore.h"
#include "xAODCore/ShallowCopy.h"

bool flagDisableHistograms = true;

SCTHitEfficiency::SCTHitEfficiency() 
{
  std::cout<<"in SCTHitEfficiency constructor"<<std::endl;
  m_histList = new TList();
  
}
void SCTHitEfficiency::bookHists() 
{

  m_trackTree = new TTree("track", "track");
  m_trackTree->Branch("run",        &m_branch.run,     "run/I");
  m_trackTree->Branch("lb",         &m_branch.lb,      "lb/I");
  m_trackTree->Branch("event",      &m_branch.event,   "event/I");
  m_trackTree->Branch("bcid",       &m_branch.bcid,    "bcid/I");
  m_trackTree->Branch("track",      &m_branch.track,   "track/I");
  m_trackTree->Branch("p",          &m_branch.p,       "p/F");
  m_trackTree->Branch("pt",         &m_branch.pT,      "pt/F");
  m_trackTree->Branch("pti",        &m_branch.pti,     "pti/F");
  m_trackTree->Branch("m",          &m_branch.m,       "m/F");
  m_trackTree->Branch("d0",         &m_branch.d0,      "d0/F");
  m_trackTree->Branch("z0",         &m_branch.z0,      "z0/F");
  m_trackTree->Branch("angle",      &m_branch.angle,   "angle/F");
  m_trackTree->Branch("chi2",       &m_branch.chi2,    "chi2/F");
  m_trackTree->Branch("dof",        &m_branch.dof,    "dof/F");
  m_trackTree->Branch("trkphi",     &m_branch.trkphi,  "trkphi/F");
  m_trackTree->Branch("trketa",     &m_branch.trketa,  "trketa/F");
  m_trackTree->Branch("nhit_SCT",   &m_branch.nhit_SCT,"nhit_SCT/I");
  m_trackTree->Branch("nhit_Trt",   &m_branch.nhit_Trt,"nhit_Trt/I");
  m_trackTree->Branch("nhit_Pixel", &m_branch.nhit_Pixel,"nhit_Pixel/I");  
  m_trackTree->Branch("nout_SCT",   &m_branch.nout_SCT,"nout_SCT/I");
  m_trackTree->Branch("nout_Trt",   &m_branch.nout_Trt,"nout_Trt/I");
  m_trackTree->Branch("nout_Pixel", &m_branch.nout_Pixel,"nout_Pixel/I");  
  m_trackTree->Branch("flagb3",     &m_branch.flagB3Unbiased,"flagb3/I");
  m_trackTree->Branch("hit_bec",    &m_branch.hit_bec    );
  m_trackTree->Branch("hit_layer",  &m_branch.hit_layer  );
  m_trackTree->Branch("hit_eta",    &m_branch.hit_eta    );
  m_trackTree->Branch("hit_phi",    &m_branch.hit_phi    );
  m_trackTree->Branch("hit_side",   &m_branch.hit_side   );
  m_trackTree->Branch("hit_clusize",&m_branch.hit_clusize);
  m_trackTree->Branch("hit_strip",  &m_branch.hit_edgeStrip);
  m_trackTree->Branch("hit_localx", &m_branch.hit_localx );
  m_trackTree->Branch("hit_localy", &m_branch.hit_localy );
  m_trackTree->Branch("hit_resx",   &m_branch.hit_residualx);
  m_trackTree->Branch("hit_localphi",   &m_branch.hit_localphi);
  m_trackTree->Branch("hit_localtheta", &m_branch.hit_localtheta);
  m_trackTree->Branch("hit_chi2", &m_branch.hit_chi2);
  m_trackTree->Branch("hit_residual",   &m_branch.hit_residual);
  m_trackTree->Branch("hit_resy",   &m_branch.hit_residualy);
  m_trackTree->Branch("hit_pullx",  &m_branch.hit_pullx  );
  m_trackTree->Branch("hit_pully",  &m_branch.hit_pully  );
  m_trackTree->Branch("hole_bec",   &m_branch.hole_bec   );
  m_trackTree->Branch("hole_layer", &m_branch.hole_layer );
  m_trackTree->Branch("hole_eta",   &m_branch.hole_eta   );
  m_trackTree->Branch("hole_phi",   &m_branch.hole_phi   );
  m_trackTree->Branch("hole_side",  &m_branch.hole_side  );
  m_trackTree->Branch("hole_localx",&m_branch.hole_localx);
  m_trackTree->Branch("hole_localy",&m_branch.hole_localy);
  m_trackTree->Branch("hole_localphi",   &m_branch.hole_localphi);
  m_trackTree->Branch("hole_localtheta", &m_branch.hole_localtheta);
  m_trackTree->Branch("hole_chi2", &m_branch.hole_chi2);
  m_trackTree->Branch("hole_residual",   &m_branch.hole_residual);
  m_trackTree->Branch("hole_resx",   &m_branch.hole_residualx);
  m_trackTree->Branch("hole_resy",   &m_branch.hole_residualy);
  m_trackTree->Branch("hole_pullx",  &m_branch.hole_pullx  );
  m_trackTree->Branch("hole_pully",  &m_branch.hole_pully  );
  m_trackTree->Branch("nhole",      &m_branch.nhole,   "nhole/I");
  m_trackTree->Branch("nhit",       &m_branch.nhit,    "nhit/I");
  m_histList->Add(m_trackTree);

  m_eventTree = new TTree("event", "event");
  m_eventTree->Branch("run",          &m_branch.run,         "run/I");
  m_eventTree->Branch("lb",           &m_branch.lb,          "lb/I");
  m_eventTree->Branch("event",        &m_branch.event,       "event/I");
  m_eventTree->Branch("ntrack",       &m_branch.ntrack,      "ntrack/I");
  m_eventTree->Branch("ntrack_aftercut",       &m_branch.ntrack_aftercut,      "ntrack_aftercut/I");
  m_eventTree->Branch("bsErr_bec",    &m_branch.bsErr_bec);
  m_eventTree->Branch("bsErr_layer",  &m_branch.bsErr_layer);
  m_eventTree->Branch("bsErr_phi",    &m_branch.bsErr_phi);
  m_eventTree->Branch("bsErr_eta",    &m_branch.bsErr_eta);
  m_eventTree->Branch("bsErr_side",   &m_branch.bsErr_side);
  m_eventTree->Branch("bsErr_type",   &m_branch.bsErr_type);
  m_histList->Add(m_eventTree);
  
}

TList* 
SCTHitEfficiency::getHists() 
{
  return m_histList;
}


void 
SCTHitEfficiency::execute(xAOD::TEvent* thisEvent) 
{
  
  // get track container of interest
  const xAOD::TrackParticleContainer* recoTracks = 0;
  if ( !thisEvent->retrieve( recoTracks, "InDetTrackParticles" ).isSuccess() ){ // retrieve arguments: container type, container key
    Error("execute()", "Failed to retrieve Reconstructed Track container. Exiting." );
    return;//// EL::StatusCode::FAILURE;
  }
  const xAOD::EventInfo* eventInfo = 0;
  if( ! thisEvent->retrieve( eventInfo, "EventInfo").isSuccess() ){
    std::cout<<"Failed to retrieve eventInfo"<<std::endl;
    return;
  }
  /*  if( recoTracks->size()==0 ) {
    return;
    }*/

  m_branch.run  = eventInfo->runNumber();
  m_branch.lb   = eventInfo->lumiBlock();
  m_branch.bcid = eventInfo->bcid();

  m_branch.event = eventInfo->eventNumber();
  m_branch.ntrack = recoTracks->size();
  m_branch.ntrack_aftercut = 0;

  m_branch.track = recoTracks->size();

  // Get BS errors

  m_branch.bsErr_bec   = eventInfo->auxdata<std::vector<int> >("_SCT_BSErr_bec");
  m_branch.bsErr_layer = eventInfo->auxdata<std::vector<int> >("_SCT_BSErr_layer");
  m_branch.bsErr_eta   = eventInfo->auxdata<std::vector<int> >("_SCT_BSErr_eta");
  m_branch.bsErr_phi   = eventInfo->auxdata<std::vector<int> >("_SCT_BSErr_phi");
  m_branch.bsErr_side  = eventInfo->auxdata<std::vector<int> >("_SCT_BSErr_side");
  m_branch.bsErr_type  = eventInfo->auxdata<std::vector<int> >("_SCT_BSErr_type");


  // loop over the tracks in the container
  for( xAOD::TrackParticleContainer::const_iterator recoTrk_itr = recoTracks->begin();
          recoTrk_itr != recoTracks->end(); recoTrk_itr++) {
    
    // Make your track selection here

    // Analysis start

    m_branch.nhit=(int)((*recoTrk_itr)->auxdata<uint8_t>("numberOfSCTHits"));
    m_branch.nhole=(int)((*recoTrk_itr)->auxdata<uint8_t>("numberOfSCTHoles"));
    
    typedef std::vector<ElementLink< xAOD::TrackStateValidationContainer > > MeasurementsOnTrack;
    typedef std::vector<ElementLink< xAOD::TrackStateValidationContainer > >::const_iterator MeasurementsOnTrackIter;
    static const char* measurementNames = "msosLink";   //Note the prefix could change
    //    static const char* measurementNames = "IDDET1_msosLink";   //Note the prefix could change


    // Check if there are MSOS attached
    if( ! (*recoTrk_itr)->isAvailable< MeasurementsOnTrack >( measurementNames ) ) {
      std::cout<<" Measurements on track not available"<<std::endl;
      continue;
    }

    // Get track information
    m_branch.pT = (*recoTrk_itr)->pt()*0.001;
    m_branch.p  = (*recoTrk_itr)->p4().Rho()*0.001;
    m_branch.m  = (*recoTrk_itr)->m();
    m_branch.pti = (*recoTrk_itr)->qOverP();
    m_branch.d0 = (float)((*recoTrk_itr)->auxdata<float>("d0"));
    m_branch.z0 = (float)((*recoTrk_itr)->auxdata<float>("z0"));
    m_branch.chi2 = (float)((*recoTrk_itr)->auxdata<float>("chiSquared"));
    m_branch.dof = (float)((*recoTrk_itr)->auxdata<float>("numberDoF"));
    m_branch.trkphi = (float)((*recoTrk_itr)->auxdata<float>("phi"));
    m_branch.trketa = -(float)log(tan(((*recoTrk_itr)->auxdata<float>("theta"))/2));
    m_branch.trktheta = (float)((*recoTrk_itr)->auxdata<float>("theta"));
    m_branch.nhit_SCT = (int)((*recoTrk_itr)->auxdata<uint8_t>("numberOfSCTHits"));
    m_branch.nhit_Pixel = (int)((*recoTrk_itr)->auxdata<uint8_t>("numberOfPixelHits"));
    m_branch.nhit_Trt = (int)((*recoTrk_itr)->auxdata<uint8_t>("numberOfTRTHits"));
    m_branch.nout_SCT = (int)((*recoTrk_itr)->auxdata<uint8_t>("numberOfSCTOutliers"));
    m_branch.nout_Pixel = (int)((*recoTrk_itr)->auxdata<uint8_t>("numberOfPixelOutliers"));
    m_branch.nout_Trt = (int)((*recoTrk_itr)->auxdata<uint8_t>("numberOfTRTOutliers"));
    //    m_branch.trktheta = (float)((*recoTrk_itr)->auxdata<float>("theta"));

    // Initialise hit / hole vectors
    m_branch.hit_bec.clear();
    m_branch.hit_layer.clear();
    m_branch.hit_eta.clear();
    m_branch.hit_phi.clear();
    m_branch.hit_side.clear();
    m_branch.hit_localx.clear();
    m_branch.hit_localy.clear();
    m_branch.hit_residualx.clear();
    m_branch.hit_residualy.clear();
    m_branch.hit_pullx.clear();
    m_branch.hit_pully.clear();
    m_branch.hit_localphi.clear();
    m_branch.hit_localtheta.clear();
    m_branch.hit_chi2.clear();
    m_branch.hit_residual.clear();
    m_branch.hit_clusize.clear();
    m_branch.hit_edgeStrip.clear();
    m_branch.hole_bec.clear();
    m_branch.hole_layer.clear();
    m_branch.hole_eta.clear();
    m_branch.hole_phi.clear();
    m_branch.hole_side.clear();
    m_branch.hole_localx.clear();
    m_branch.hole_localy.clear();
    m_branch.hole_residualx.clear();
    m_branch.hole_residualy.clear();
    m_branch.hole_pullx.clear();
    m_branch.hole_pully.clear();
    m_branch.hole_localphi.clear();
    m_branch.hole_localtheta.clear();
    m_branch.hole_chi2.clear();
    m_branch.hole_residual.clear();
    
    // Get the MSOS's
    const MeasurementsOnTrack& measurementsOnTrack = (*recoTrk_itr)->auxdata< MeasurementsOnTrack >( measurementNames );

    // Loop over track TrackStateValidation's
    int nClusOnTrack = 0;
    int nSctHoles = 0;
    int nSiHits  = 0;
    int nSctOtherHits = 0;

    for( MeasurementsOnTrackIter msos_iter = measurementsOnTrack.begin();
	 msos_iter != measurementsOnTrack.end(); ++msos_iter) {  
      //Check if the element link is valid
      if( ! (*msos_iter).isValid() ) {
	continue;
      }

      const xAOD::TrackStateValidation* msos = *(*msos_iter); 
      if( (int)(msos->detType()) != 2 ){
	if( msos->type() == 0 ){
	  nSiHits++;
	}
	continue;// 1 = Pixel, 2 = SCT
      }

      int bec,layer,eta,phi,side;
      float localx, localy;
      bec   = (int)((((uint64_t)0x3  << 57) & (msos->detElementId())) >> 57);
      bec = (bec - 1)*2;
      layer = (int)((((uint64_t)0xf  << 53) & (msos->detElementId())) >> 53);
      phi   = (int)((((uint64_t)0x3f << 47) & (msos->detElementId())) >> 47);
      eta   = (int)((((uint64_t)0xf  << 43) & (msos->detElementId())) >> 43) - 6;

      side  = (int)((((uint64_t)0x1  << 42) & (msos->detElementId())) >> 42);
      localx = msos->localX();
      localy = msos->localY();
      
      float residualx = msos->unbiasedResidualX();
      float residualy = msos->unbiasedResidualY();
      float pullx     = msos->unbiasedPullX();
      float pully     = msos->unbiasedPullY();
      //      double chi2 = msos->chi2();
      //      float residual = msos->residual();
      float localphi = 0.;
      float localtheta = 0.;
      if(msos->localPhi() > TMath::Pi()/2.){
	localphi  = msos->localPhi()-TMath::Pi();
      }else{
	localphi  = msos->localPhi()+TMath::Pi();
      }
      
      if(msos->localTheta() > TMath::Pi()/2.){
	localtheta  = msos->localPhi()-TMath::Pi();
      }else{
	localtheta  = msos->localTheta()+TMath::Pi();
      }

      if( msos->type() == 6 ){
	m_branch.hole_bec.push_back(bec  );
	m_branch.hole_layer.push_back(layer);
	m_branch.hole_phi.push_back(phi  );
	m_branch.hole_eta.push_back(eta  );
	m_branch.hole_side.push_back(side );
	m_branch.hole_localx.push_back(localx);
	m_branch.hole_localy.push_back(localy);
	m_branch.hole_residualx.push_back(residualx);
	m_branch.hole_residualy.push_back(residualy);
	m_branch.hole_pullx.push_back(pullx);
	m_branch.hole_pully.push_back(pully);
	m_branch.hole_localphi.push_back(localphi);
	m_branch.hole_localtheta.push_back(localtheta);
	//	m_branch.hole_chi2.push_back(chi2);
	//	m_branch.hole_residual.push_back(residual);
	
	if( ! (bec == 0 && layer == 0) ){
	  nSctHoles++;
	}

      }else if( msos->type() == 0 || msos->type() == 5 ){
	m_branch.hit_bec.push_back(bec  );
	m_branch.hit_layer.push_back(layer);
	m_branch.hit_phi.push_back(phi  );
	m_branch.hit_eta.push_back(eta  );
	m_branch.hit_side.push_back(side );
	m_branch.hit_localx.push_back(localx);
	m_branch.hit_localy.push_back(localy);
	m_branch.hit_residualx.push_back(residualx);
	m_branch.hit_residualy.push_back(residualy);
	m_branch.hit_pullx.push_back(pullx);
	m_branch.hit_pully.push_back(pully);
	m_branch.hit_localphi.push_back(localphi);
	m_branch.hit_localtheta.push_back(localtheta);
	//	m_branch.hit_chi2.push_back(chi2);
	//	m_branch.hit_residual.push_back(residual);
	
	if( ! (bec == 0 && layer == 0) ){
	  nSiHits++;
	  nSctOtherHits++;
	}
      }
      
      //Get SCT cluster
      if(  msos->trackMeasurementValidationLink().isValid() && *(msos->trackMeasurementValidationLink()) ){
	
        // Access a dressed member    
	nClusOnTrack++;
	
	const xAOD::TrackMeasurementValidation* sctCluster =  *(msos->trackMeasurementValidationLink());        

	int clusize = (sctCluster)->auxdataConst<int>("SiWidth");
	std::vector<int> rdoStrip = (sctCluster)->auxdataConst<std::vector<int>>("rdo_strip");

	m_branch.hit_clusize.push_back(clusize);
	m_branch.hit_edgeStrip.push_back(rdoStrip.at(0));

      }

    }

    if( nSiHits >= 7 && nSctOtherHits >= 6 && nSctHoles <= 1 ) m_branch.flagB3Unbiased = 1;
    else m_branch.flagB3Unbiased = 0;
    m_trackTree->Fill();
    
    //m_branch.track++;

  }
  
  m_eventTree->Fill();
  
}

void 
SCTHitEfficiency::finalize() 
{
  
}



