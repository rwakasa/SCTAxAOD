#include <iostream>
#include "SCTAna/SCTTrackPlots.h"
 
// EDM includes: - if move to header file will not compile?
#include "xAODTracking/TrackMeasurementValidationContainer.h"
#include "xAODTracking/TrackStateValidationContainer.h"

#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTracking/TrackParticleAuxContainer.h"
 
#include "xAODTracking/SCTRawHitValidation.h"
#include "xAODTracking/SCTRawHitValidationContainer.h"
#include "xAODTracking/SCTRawHitValidationAuxContainer.h"
//#include "xAODRootAccess/TStore.h"
//#include "xAODCore/ShallowCopy.h"
 
#include "InDetTrackSelectionTool/InDetTrackSelectionTool.h"

// GRL
#include "GoodRunsLists/GoodRunsListSelectionTool.h"
#include <TSystem.h>

#include "xAODEventInfo/EventInfo.h"
 
SCTTrackPlots::SCTTrackPlots()
{
  std::cout<<"in SCTTrackPlots constructor"<<std::endl;
  m_histList = new TList();
 
}
void SCTTrackPlots::bookHists()
{
  m_nTracks = new TH1D("nTracks","nTracks", 5001, -0.5, 5000.5);
  m_histList->Add(m_nTracks);
  m_nLooseTracks = new TH1D("nLooseTracks","nLooseTracks", 5001, -0.5, 5000.5);
  m_histList->Add(m_nLooseTracks);
  m_nLoosePrimaryTracks = new TH1D("nLoosePrimaryTracks","nLoosePrimaryTracks", 5001, -0.5, 5000.5);
  m_histList->Add(m_nLoosePrimaryTracks);
  m_nTightPrimaryTracks = new TH1D("nTightPrimaryTracks", "nTightPrimaryTracks", 5001, -0.5, 5000.5);
  m_histList->Add(m_nTightPrimaryTracks);

  // GRL
  m_grl = new GoodRunsListSelectionTool("GoodRunsListSelectionTool");
  const char* GRLFilePath = "$ROOTCOREBIN/data/SCTAna/data16_13TeV.periodAllYear_DetStatus-v88-pro20-21_DQDefects-00-02-04_PHYS_StandardGRL_All_Good.xml";
  const char* fullGRLFilePath = gSystem->ExpandPathName(GRLFilePath);
  std::vector<std::string> vecStringGRL;
  vecStringGRL.push_back(fullGRLFilePath);
  m_grl->setProperty("GoodRunsListVec", vecStringGRL).isSuccess();
  m_grl->setProperty("PassThrough", false).isSuccess(); // if true (default) will ignore result of GRL and will just pass all events
  m_grl->initialize().isSuccess();

  // m_nEvents_LB = new TH1D("nEvents_LB", "nEvents_LB", 31, 950-0.5, 980+0.5);
  // m_histList->Add(m_nEvents_LB);
  // m_nTracks_LB = new TH1D("nTracks_LB", "nTracks_LB", 31, 950-0.5, 980+0.5);
  // m_histList->Add(m_nTracks_LB);
  // m_nLooseTracks_LB = new TH1D("nLooseTracks_LB", "nLooseTracks_LB", 31, 950-0.5, 980+0.5);
  // m_histList->Add(m_nLooseTracks_LB);
  // m_nLoosePrimaryTracks_LB = new TH1D("nLoosePrimaryTracks_LB", "nLoosePrimaryTracks_LB", 31, 950-0.5, 980+0.5);
  // m_histList->Add(m_nLoosePrimaryTracks_LB);
  // m_nTightPrimaryTracks_LB = new TH1D("nTightPrimaryTracks_LB", "nTightPrimaryTracks_LB", 31, 950-0.5, 980+0.5);
  // m_histList->Add(m_nTightPrimaryTracks_LB);
  // m_nTracksBarrel_LB = new TH1D("nTracksBarrel_LB", "nTracksBarrel_LB", 31, 950-0.5, 980+0.5);
  // m_histList->Add(m_nTracksBarrel_LB);
  // m_nLooseTracksBarrel_LB = new TH1D("nLooseTracksBarrel_LB", "nLooseTracksBarrel_LB", 31, 950-0.5, 980+0.5);
  // m_histList->Add(m_nLooseTracksBarrel_LB);
  // m_nLoosePrimaryTracksBarrel_LB = new TH1D("nLoosePrimaryTracksBarrel_LB", "nLoosePrimaryTracksBarrel_LB", 31, 950-0.5, 980+0.5);
  // m_histList->Add(m_nLoosePrimaryTracksBarrel_LB);
  // m_nTightPrimaryTracksBarrel_LB = new TH1D("nTightPrimaryTracksBarrel_LB", "nTightPrimaryTracksBarrel_LB", 31, 950-0.5, 980+0.5);
  // m_histList->Add(m_nTightPrimaryTracksBarrel_LB);
  // m_numberOfSCTHitsBarrel_LB = new TH2D("numberOfSCTHitsBarrel_LB", "numberOfSCTHitsBarrel_LB", 31, 950-0.5, 980+0.5, 21, -0.5, 20.5);
  // m_histList->Add(m_numberOfSCTHitsBarrel_LB);
  // m_numberOfSCTHolesBarrel_LB = new TH2D("numberOfSCTHolesBarrel_LB", "numberOfSCTHolesBarrel_LB", 31, 950-0.5, 980+0.5, 21, -0.5, 20.5);
  // m_histList->Add(m_numberOfSCTHolesBarrel_LB);

  // m_SiWidthBarrel3_LB[0] = new TH2D("SiWidthBarrel3Eta1_LB", "SiWidthBarrel3Eta1_LB", 31, 950-0.5, 980+0.5, 201, -0.5, 200.5);
  // m_histList->Add(m_SiWidthBarrel3_LB[0]);
  // m_SiWidthBarrel3_LB[1] = new TH2D("SiWidthBarrel3Eta2_LB", "SiWidthBarrel3Eta2_LB", 31, 950-0.5, 980+0.5, 201, -0.5, 200.5);
  // m_histList->Add(m_SiWidthBarrel3_LB[1]);
  // m_SiWidthBarrel3_LB[2] = new TH2D("SiWidthBarrel3Eta3_LB", "SiWidthBarrel3Eta3_LB", 31, 950-0.5, 980+0.5, 201, -0.5, 200.5);
  // m_histList->Add(m_SiWidthBarrel3_LB[2]);
  // m_SiWidthBarrel3_LB[3] = new TH2D("SiWidthBarrel3Eta4_LB", "SiWidthBarrel3Eta4_LB", 31, 950-0.5, 980+0.5, 201, -0.5, 200.5);
  // m_histList->Add(m_SiWidthBarrel3_LB[3]);
  // m_SiWidthBarrel3_LB[4] = new TH2D("SiWidthBarrel3Eta5_LB", "SiWidthBarrel3Eta5_LB", 31, 950-0.5, 980+0.5, 201, -0.5, 200.5);
  // m_histList->Add(m_SiWidthBarrel3_LB[4]);
  // m_SiWidthBarrel3_LB[5] = new TH2D("SiWidthBarrel3Eta6_LB", "SiWidthBarrel3Eta6_LB", 31, 950-0.5, 980+0.5, 201, -0.5, 200.5);
  // m_histList->Add(m_SiWidthBarrel3_LB[5]);

  // m_pt_LB = new TH2D("pt_LB", "pt_LB", 31, 950-0.5, 980+0.5, 1000, 0., 100.);
  // m_histList->Add(m_pt_LB);

  m_eta_phi = new TH2D("eta_phi", "eta_phi", 100, -2.5, 2.5, 100, -TMath::Pi(), +TMath::Pi());
  m_histList->Add(m_eta_phi);
 
  m_eta_phi_numberOfSCTHits = new TH2D("eta_phi_numberOfSCTHits", "eta_phi_numberOfSCTHits", 100, -2.5, 2.5, 100, -TMath::Pi(), +TMath::Pi());
  m_histList->Add(m_eta_phi_numberOfSCTHits);  
 
  m_eta_phi_numberOfSCTOutliers = new TH2D("eta_phi_numberOfSCTOutliers", "eta_phi_numberOfSCTOutliers", 100, -2.5, 2.5, 100, -TMath::Pi(), +TMath::Pi());
  m_histList->Add(m_eta_phi_numberOfSCTOutliers);  
 
  m_eta_phi_numberOfSCTHoles = new TH2D("eta_phi_numberOfSCTHoles", "eta_phi_numberOfSCTHoles", 100, -2.5, 2.5, 100, -TMath::Pi(), +TMath::Pi());
  m_histList->Add(m_eta_phi_numberOfSCTHoles);  
 
  m_eta_phi_numberOfSCTDoubleHoles = new TH2D("eta_phi_numberOfSCTDoubleHoles", "eta_phi_numberOfSCTDoubleHoles", 100, -2.5, 2.5, 100, -TMath::Pi(), +TMath::Pi());
  m_histList->Add(m_eta_phi_numberOfSCTDoubleHoles);  
 
  m_eta_phi_numberOfSCTSharedHits = new TH2D("eta_phi_numberOfSCTSharedHits", "eta_phi_numberOfSCTSharedHits", 100, -2.5, 2.5, 100, -TMath::Pi(), +TMath::Pi());
  m_histList->Add(m_eta_phi_numberOfSCTSharedHits);  
 
  m_eta_phi_numberOfSCTDeadSensors = new TH2D("eta_phi_numberOfSCTDeadSensors", "eta_phi_numberOfSCTDeadSensors", 100, -2.5, 2.5, 100, -TMath::Pi(), +TMath::Pi());
  m_histList->Add(m_eta_phi_numberOfSCTDeadSensors);  
 
  m_eta_phi_numberOfSCTSpoiltHits = new TH2D("eta_phi_numberOfSCTSpoilHits", "eta_phi_numberOfSCTSpoilHits", 100, -2.5, 2.5, 100, -TMath::Pi(), +TMath::Pi());
  m_histList->Add(m_eta_phi_numberOfSCTSpoiltHits);  
 
  m_numberOfSCTHitsActualInteractionsN = new TH2D("numberOfSCTHitsActualInteractionsN", "numberOfSCTHitsActualInteractionsN", 700, 0., 70., 31, -0.5, 30.5);
  m_histList->Add(m_numberOfSCTHitsActualInteractionsN);  
  m_numberOfSCTHitsActualInteractionsL = new TH2D("numberOfSCTHitsActualInteractionsL", "numberOfSCTHitsActualInteractionsL", 700, 0., 70., 31, -0.5, 30.5);
  m_histList->Add(m_numberOfSCTHitsActualInteractionsL);  
  m_numberOfSCTHitsActualInteractionsLP = new TH2D("numberOfSCTHitsActualInteractionsLP", "numberOfSCTHitsActualInteractionsLP", 700, 0., 70., 31, -0.5, 30.5);
  m_histList->Add(m_numberOfSCTHitsActualInteractionsLP);  
  m_numberOfSCTHitsActualInteractionsTP = new TH2D("numberOfSCTHitsActualInteractionsTP", "numberOfSCTHitsActualInteractionsTP", 700, 0., 70., 31, -0.5, 30.5);
  m_histList->Add(m_numberOfSCTHitsActualInteractionsTP);  

  m_numberOfSCTHitsLBN = new TH2D("numberOfSCTHitsLBN", "numberOfSCTHitsLBN", 1501, -0.5, 1500.5, 31, -0.5, 30.5);
  m_histList->Add(m_numberOfSCTHitsLBN);  
  m_numberOfSCTHitsLBL = new TH2D("numberOfSCTHitsLBL", "numberOfSCTHitsLBL", 1501, -0.5, 1500.5, 31, -0.5, 30.5);
  m_histList->Add(m_numberOfSCTHitsLBL);  
  m_numberOfSCTHitsLBLP = new TH2D("numberOfSCTHitsLBLP", "numberOfSCTHitsLBLP", 1501, -0.5, 1500.5, 31, -0.5, 30.5);
  m_histList->Add(m_numberOfSCTHitsLBLP);  
  m_numberOfSCTHitsLBTP = new TH2D("numberOfSCTHitsLBTP", "numberOfSCTHitsLBTP", 1501, -0.5, 1500.5, 31, -0.5, 30.5);
  m_histList->Add(m_numberOfSCTHitsLBTP);  

  m_numberOfSCTHits = new TH1D("numberOfSCTHits", "numberOfSCTHits", 31, -0.5, 30.5);
  m_histList->Add(m_numberOfSCTHits);  
 
  m_numberOfSCTOutliers = new TH1D("numberOfSCTOutliers", "numberOfSCTOutliers", 31, -0.5, 30.5);
  m_histList->Add(m_numberOfSCTOutliers);  
 
  m_numberOfSCTHoles = new TH1D("numberOfSCTHoles", "numberOfSCTHoles", 31, -0.5, 30.5);
  m_histList->Add(m_numberOfSCTHoles);  
 
  m_numberOfSCTDoubleHoles = new TH1D("numberOfSCTDoubleHoles", "numberOfSCTDoubleHoles", 31, -0.5, 30.5);
  m_histList->Add(m_numberOfSCTDoubleHoles);  
 
  m_numberOfSCTSharedHits = new TH1D("numberOfSCTSharedHits", "numberOfSCTSharedHits", 31, -0.5, 30.5);
  m_histList->Add(m_numberOfSCTSharedHits);  
 
  m_numberOfSCTDeadSensors = new TH1D("numberOfSCTDeadSensors", "numberOfSCTDeadSensors", 31, -0.5, 30.5);
  m_histList->Add(m_numberOfSCTDeadSensors);  
 
  m_numberOfSCTSpoiltHits = new TH1D("numberOfSCTSpoilHits", "numberOfSCTSpoilHits", 31, -0.5, 30.5);
  m_histList->Add(m_numberOfSCTSpoiltHits);  
 
  m_prob = new TH1D("prob", "prob", 100, 0., 1.);
  m_histList->Add(m_prob);  
 
  //  m_RDOs = new TH1D("RDOs", "nRDOs", 200001, -0.5, 200000.5);
  //  m_histList->Add(m_RDOs);  
  m_Clusters = new TH1D("Clusters", "nClusters", 100001, -0.5, 10000.5);
  m_histList->Add(m_Clusters);  
 
  m_selectionToolLoose = new InDet::InDetTrackSelectionTool("TrackSelectionLoose");
  m_selectionToolLoose->setProperty("CutLevel", "Loose").isSuccess(); // set tool to apply the pre-defined "Loose" cuts        
  m_selectionToolLoose->initialize().isSuccess();
 
  m_selectionToolLoosePrimary = new InDet::InDetTrackSelectionTool("TrackSelectionLoosePrimary");
  m_selectionToolLoosePrimary->setProperty("CutLevel", "LoosePrimary").isSuccess(); // set tool to apply the pre-defined "LoosePrimary" cuts        
  m_selectionToolLoosePrimary->initialize().isSuccess();
 
  m_selectionToolTightPrimary = new InDet::InDetTrackSelectionTool("TrackSelectionTightPrimary");
  m_selectionToolTightPrimary->setProperty("CutLevel", "TightPrimary").isSuccess(); // set tool to apply the pre-defined "TightPrimary" cuts        
  m_selectionToolTightPrimary->initialize().isSuccess();
}
 
TList*
SCTTrackPlots::getHists()
{
  return m_histList;
}

void
SCTTrackPlots::execute(xAOD::TEvent* thisEvent)
{
  //  typedef std::vector<ElementLink< xAOD::TrackStateValidationContainer > > MeasurementsOnTrack;
  //  typedef std::vector<ElementLink< xAOD::TrackStateValidationContainer > >::const_iterator MeasurementsOnTrackIter;

  const xAOD::EventInfo* eventInfo = 0;
  if( ! thisEvent->retrieve( eventInfo, "EventInfo").isSuccess() ){
    std::cout<<"Failed to retrieve eventInfo"<<std::endl;
    return;
  }
  int lumiBlock = eventInfo->lumiBlock();
  //  if(lumiBlock>=958 and lumiBlock<=974) return; //////

  float actualInteractionsPerCrossing = eventInfo->auxdata<float>("actualInteractionsPerCrossing");

  // check if the event is data or MC
  // (many tools are applied either to data or MC)
  bool isMC = false;
  // check if the event is MC
  if(eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) ){
    isMC = true; // can do something with this later
  }
  // if data check if event passes GRL
  if(!isMC) { // it's data!
    if(!m_grl->passRunLB(*eventInfo)){
      return; // go to next event
    }
  } // end if not MC
 
  // get track container of interest
  const xAOD::TrackParticleContainer* recoTracks = 0;
  if ( !thisEvent->retrieve( recoTracks, "InDetTrackParticles" ).isSuccess() ){ // retrieve arguments: container type, container key
    Error("execute()", "Failed to retrieve Reconstructed Track container. Exiting." );
    return;
  }

  unsigned int nTracks = 0;
  unsigned int nLooseTracks = 0;
  unsigned int nLoosePrimaryTracks = 0;
  unsigned int nTightPrimaryTracks = 0;
  unsigned int nTracksBarrel = 0;
  unsigned int nLooseTracksBarrel = 0;
  unsigned int nLooseTracksBarrel1GeV = 0;
  unsigned int nLoosePrimaryTracksBarrel = 0;
  unsigned int nTightPrimaryTracksBarrel = 0;
  // // loop over the tracks in the container
 for(xAOD::TrackParticleContainer::const_iterator recoTrk_itr = recoTracks->begin();
     recoTrk_itr != recoTracks->end(); recoTrk_itr++) {
    double eta = (*recoTrk_itr)->eta();
    bool isBarrel = (fabs(eta)<1.65);
    int numberOfSCTHits        =(int)((*recoTrk_itr)->auxdata<uint8_t>("numberOfSCTHits"));

    //    if(isBarrel) m_pt_LB->Fill(lumiBlock, (*recoTrk_itr)->pt()/1000.);

    nTracks++;
    if(isBarrel) nTracksBarrel++;

    m_numberOfSCTHitsActualInteractionsN->Fill(actualInteractionsPerCrossing, numberOfSCTHits);
    m_numberOfSCTHitsLBN->Fill(lumiBlock, numberOfSCTHits);

    if(m_selectionToolLoosePrimary->accept(*recoTrk_itr)) {
      nLoosePrimaryTracks++;
      if(isBarrel) nLoosePrimaryTracksBarrel++;

      m_numberOfSCTHitsActualInteractionsLP->Fill(actualInteractionsPerCrossing, numberOfSCTHits);
      m_numberOfSCTHitsLBLP->Fill(lumiBlock, numberOfSCTHits);
    }
    if(m_selectionToolTightPrimary->accept(*recoTrk_itr)) {
      nTightPrimaryTracks++;
      if(isBarrel) nTightPrimaryTracksBarrel++;

      m_numberOfSCTHitsActualInteractionsTP->Fill(actualInteractionsPerCrossing, numberOfSCTHits);
      m_numberOfSCTHitsLBTP->Fill(lumiBlock, numberOfSCTHits);
    }
   
     if((*recoTrk_itr)->isAvailable<float>("truthMatchProbability")) {
       m_prob->Fill((*recoTrk_itr)->auxdata<float>("truthMatchProbability"));
     }

   if(m_selectionToolLoose->accept(*recoTrk_itr)) {
      nLooseTracks++;
      if(isBarrel) nLooseTracksBarrel++;

      m_numberOfSCTHitsActualInteractionsL->Fill(actualInteractionsPerCrossing, numberOfSCTHits);
      m_numberOfSCTHitsLBL->Fill(lumiBlock, numberOfSCTHits);

      if((*recoTrk_itr)->pt()>1000.) {
  	if(isBarrel) nLooseTracksBarrel1GeV++;

        double phi = (*recoTrk_itr)->phi();
        m_eta_phi->Fill(eta, phi);
 
	//        int numberOfSCTHits        =(int)((*recoTrk_itr)->auxdata<uint8_t>("numberOfSCTHits"));
        m_eta_phi_numberOfSCTHits->Fill(eta, phi, numberOfSCTHits);
        m_numberOfSCTHits->Fill(numberOfSCTHits);
	//  	if(isBarrel) m_numberOfSCTHitsBarrel_LB->Fill(lumiBlock, numberOfSCTHits);
       
        int numberOfSCTOutliers    =(int)((*recoTrk_itr)->auxdata<uint8_t>("numberOfSCTOutliers"));
        m_eta_phi_numberOfSCTOutliers->Fill(eta, phi, numberOfSCTOutliers);
        m_numberOfSCTOutliers->Fill(numberOfSCTOutliers);
 
        int numberOfSCTHoles       =(int)((*recoTrk_itr)->auxdata<uint8_t>("numberOfSCTHoles"));
        m_eta_phi_numberOfSCTHoles->Fill(eta, phi, numberOfSCTHoles);
        m_numberOfSCTHoles->Fill(numberOfSCTHoles);
	//  	if(isBarrel) m_numberOfSCTHolesBarrel_LB->Fill(lumiBlock, numberOfSCTHoles);
 
        int numberOfSCTDoubleHoles =(int)((*recoTrk_itr)->auxdata<uint8_t>("numberOfSCTDoubleHoles"));
        m_eta_phi_numberOfSCTDoubleHoles->Fill(eta, phi, numberOfSCTDoubleHoles);
        m_numberOfSCTDoubleHoles->Fill(numberOfSCTDoubleHoles);
 
        int numberOfSCTSharedHits  =(int)((*recoTrk_itr)->auxdata<uint8_t>("numberOfSCTSharedHits"));
        m_eta_phi_numberOfSCTSharedHits->Fill(eta, phi, numberOfSCTSharedHits);
        m_numberOfSCTSharedHits->Fill(numberOfSCTSharedHits);
 
        int numberOfSCTDeadSensors =(int)((*recoTrk_itr)->auxdata<uint8_t>("numberOfSCTDeadSensors"));
        m_eta_phi_numberOfSCTDeadSensors->Fill(eta, phi, numberOfSCTDeadSensors);
        m_numberOfSCTDeadSensors->Fill(numberOfSCTDeadSensors);
 
        int numberOfSCTSpoiltHits  =(int)((*recoTrk_itr)->auxdata<uint8_t>("numberOfSCTSpoiltHits"));  
        m_eta_phi_numberOfSCTSpoiltHits->Fill(eta, phi, numberOfSCTSpoiltHits);
        m_numberOfSCTSpoiltHits->Fill(numberOfSCTSpoiltHits);

  // 	/*
  // 	if(isBarrel) {
  // 	  // Check if there are MSOS attached
  // 	  if( ! (*recoTrk_itr)->isAvailable< MeasurementsOnTrack >("msosLink") ) {
  // 	    std::cout<<" Measurements on track not available"<<std::endl;
  // 	    continue;
  // 	  }

  // 	  // Get the MSOS's
  // 	  const MeasurementsOnTrack& measurementsOnTrack = (*recoTrk_itr)->auxdata< MeasurementsOnTrack >("msosLink");
  // 	  // Loop over track TrackStateValidation's
  // 	  for(MeasurementsOnTrackIter msos_iter = measurementsOnTrack.begin();
  // 	      msos_iter != measurementsOnTrack.end(); ++msos_iter) {  
  // 	    //Check if the element link is valid
  // 	    if(not (*msos_iter).isValid()) continue;
  // 	    const xAOD::TrackStateValidation* msos = *(*msos_iter); 

  // 	    //Get SCT cluster
  // 	    if((int)(msos->detType())!= 2) continue;
  // 	    if(not msos->trackMeasurementValidationLink().isValid()) continue;
  // 	    if(not *(msos->trackMeasurementValidationLink())) continue;

  // 	    const xAOD::TrackMeasurementValidation* sctCluster = *(msos->trackMeasurementValidationLink());        
	
  // 	    int bec = (sctCluster)->auxdataConst<int>("bec");
  // 	    int layer = (sctCluster)->auxdataConst<int>("layer");
  // 	    if(not (bec==0 and layer==0)) continue;

  // 	    int eta_module = (sctCluster)->auxdataConst<int>("eta_module");
  // 	    //int phi_module = (sctCluster)->auxdataConst<int>("phi_module");	  
  // 	    //int side = (sctCluster)->auxdataConst<int>("side");

  // 	    if(not sctCluster->isAvailable<int>("SiWidth")) continue; 
  // 	    int SiWidth = sctCluster->auxdataConst<int>("SiWidth");
  // 	    m_SiWidthBarrel3_LB[abs(eta_module)-1]->Fill(lumiBlock, SiWidth);
  // 	  }
  // 	*/
      }
   }
 }
  // }
  // //  std::cout << nLooseTracks << " " << nTightPrimaryTracks << std::endl;
 m_nTracks->Fill(nTracks);
 m_nLooseTracks->Fill(nLooseTracks);
 m_nLoosePrimaryTracks->Fill(nLoosePrimaryTracks);
 m_nTightPrimaryTracks->Fill(nTightPrimaryTracks);

  // m_nEvents_LB->Fill(lumiBlock);
  // m_nTracks_LB->Fill(lumiBlock, nTracks);
  // m_nLooseTracks_LB->Fill(lumiBlock, nLooseTracks);
  // m_nLoosePrimaryTracks_LB->Fill(lumiBlock, nLoosePrimaryTracks);
  // m_nTightPrimaryTracks_LB->Fill(lumiBlock, nTightPrimaryTracks);
  // m_nTracksBarrel_LB->Fill(lumiBlock, nTracksBarrel);
  // m_nLooseTracksBarrel_LB->Fill(lumiBlock, nLooseTracksBarrel);
  // m_nLooseTracksBarrel1GeV_LB->Fill(lumiBlock, nLooseTracksBarrel1GeV);
  // m_nLoosePrimaryTracksBarrel_LB->Fill(lumiBlock, nLoosePrimaryTracksBarrel);
  // m_nTightPrimaryTracksBarrel_LB->Fill(lumiBlock, nTightPrimaryTracksBarrel);
 
  // // const xAOD::SCTRawHitValidationContainer* rdos = 0;
  // // if(!thisEvent->retrieve(rdos, "SCT_RawHits").isSuccess()) {
  // //   Error("execute()", "Failed to retrieve SCT RDOs. Exiting." );
  // //   return;
  // // }
  // //  m_RDOs->Fill(rdos->size());
  // // for(xAOD::SCTRawHitValidationContainer::const_iterator rdo_itr = rdos->begin(); rdo_itr != rdos->end(); rdo_itr++) {
  // //   const xAOD::SCTRawHitValidation *rdo = *rdo_itr;
  // //   int bec = rdo->auxdataConst<int>("bec");
  // //   int layer = rdo->auxdataConst<int>("layer");
  // //   int eta_module = rdo->auxdataConst<int>("eta_module");
  // //   int phi_module = rdo->auxdataConst<int>("phi_module");
  // //   int side = rdo->auxdataConst<int>("side");
  // // }
 
  // const xAOD::TrackMeasurementValidationContainer* sctClusters = 0;
  // if ( !thisEvent->retrieve( sctClusters, "SCT_Clusters" ).isSuccess() ){ // retrieve arguments: container type, container key
  //   Error("execute()", "Failed to retrieve SCT Clusters. Exiting." );
  //   return;
  // }
  // m_Clusters->Fill(sctClusters->size());
  // // for( xAOD::TrackMeasurementValidationContainer::const_iterator cluster_itr = sctClusters->begin();
  // //      cluster_itr != sctClusters->end(); cluster_itr++)  {
  // //   const xAOD::TrackMeasurementValidation *prd = *cluster_itr;
  // //   int bec = prd->auxdataConst<int>("bec");
  // //   int layer = prd->auxdataConst<int>("layer");
  // //   int eta_module = prd->auxdataConst<int>("eta_module");
  // //   int phi_module = prd->auxdataConst<int>("phi_module");
  // //   int side = prd->auxdataConst<int>("side");
  // // }
}
 
void
SCTTrackPlots::finalize()
{
  delete m_selectionToolLoose;
  m_selectionToolLoose = 0;
  delete m_selectionToolLoosePrimary;
  m_selectionToolLoosePrimary = 0;
  delete m_selectionToolTightPrimary;
  m_selectionToolTightPrimary = 0;
}
