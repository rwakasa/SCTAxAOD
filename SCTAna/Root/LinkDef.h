#include "SCTAna/SCTEventLoop.h"
#include "SCTAna/SCTAlgTemplate.h"
#include "SCTAna/SCTClusterPlots.h"
#include "SCTAna/SCTClusterOnTrackPlots.h"
#include "SCTAna/SCTByteStreamErrorPlots.h"
#include "SCTAna/SCT_SimpleCabling.h"
#include "SCTAna/SCTEnergyLossStudyPlots.h"
#include "SCTAna/SCTLumiPlots.h"
#include "SCTAna/SCTOccupancyPlots.h"
#include "SCTAna/SCTDataSize.h"
#include "SCTAna/SCTTrackPlots.h"
#include "SCTAna/SCTHitEfficiency.h"

#ifdef __CINT__
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;
#pragma link C++ class SCTEventLoop+;
#pragma link C++ class SCTAlgTemplate+; //// this doesn't do anything - is an example for users to copy
#pragma link C++ class SCTClusterPlots+;
#pragma link C++ class SCTLumiPlots+;
#pragma link C++ class SCTClusterOnTrackPlots+;
#pragma link C++ class SCTByteStreamErrorPlots+;
#pragma link C++ class SCT_SimpleCabling+;
#pragma link C++ class SCTEnergyLossStudyPlots+;
#pragma link C++ class SCTOccupancyPlots+;
#pragma link C++ class SCTDataSize+;
#pragma link C++ class SCTTrackPlots+;
#pragma link C++ class SCTHitEfficiency+;
#endif

