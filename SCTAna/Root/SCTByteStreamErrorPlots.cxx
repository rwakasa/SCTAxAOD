#include <iostream>
#include "SCTAna/SCTByteStreamErrorPlots.h"

#include "xAODRootAccess/TStore.h"
#include "xAODCore/ShallowCopy.h"

// EDM includes: - if move to header file will not compile?
#include "xAODEventInfo/EventInfo.h"

SCTByteStreamErrorPlots::SCTByteStreamErrorPlots() 
{
  /// create a TList to hold the histograms
  m_histList = new TList();


  /// instantiate the cabling map
  m_cabling = new SCT_SimpleCabling();


  m_errorNames = new std::vector<TString>;

  m_errorNames->push_back("ByteStreamParseError");
  m_errorNames->push_back("TimeOutError");
  m_errorNames->push_back("BCIDError");
  m_errorNames->push_back("LVL1IDError");
  m_errorNames->push_back("PreambleError");
  m_errorNames->push_back("FormatterError");
  m_errorNames->push_back("TrailerError");
  m_errorNames->push_back("TrailerOverflowError");
  m_errorNames->push_back("HeaderTrailerLimitError");
  m_errorNames->push_back("ABCDError");
  m_errorNames->push_back("RawError");
  m_errorNames->push_back("MaskedLink");
  m_errorNames->push_back("RODClockError");
  m_errorNames->push_back("TruncatedROD");
  m_errorNames->push_back("ROBFragmentError");
  m_errorNames->push_back("MissingLinkHeaderError");
  m_errorNames->push_back("MaskedROD");
  // m_errorNames->push_back("FirstWordIsHit");
  // m_errorNames->push_back("StripsOutOfOrder");
  // m_errorNames->push_back("LinksOutOfOrder");
  // m_errorNames->push_back("EventIDError");

  m_eventCounter=0;
}
void SCTByteStreamErrorPlots::bookHists() 
{
  /// create histograms and Add them to m_histList 
  
  m_sctBSErrType = new TH1F("sctBSErrType","; type; Number of errors",m_errorNames->size(),-0.5,m_errorNames->size()-0.5);
  for(unsigned int ix=0; ix<m_errorNames->size(); ix++) {
    m_sctBSErrType->GetXaxis()->SetBinLabel(ix+1, (m_errorNames->at(ix)));
  }
  m_histList->Add(m_sctBSErrType);

  TString histName="";
  for (unsigned int k=0; k< m_errorNames->size(); ++k) {
    for (int j=0; j<2;++j) {
      for (int i=0; i<4; ++i) {
	histName=m_errorNames->at(k);
	histName+="MapBarrel_";
	histName+=i;
	histName+="_";
	histName+=j;
	int index = 8*k+4*j+i;
	m_sctBSErrorMapBarrel[index] = new TH2F(histName,"barrel; eta index; phi index",13,-6.5,6.5,60,-0.5,59.5);
	m_histList->Add(m_sctBSErrorMapBarrel[index]);      
      }
      for (int i=0; i<9; ++i) {
	histName=m_errorNames->at(k);
	histName+="MapECA_";
	histName+=i;
	histName+="_";
	histName+=j;
	int index = 18*k+9*j+i;
	m_sctBSErrorMapECA[index] = new TH2F(histName,"ECA; eta index; phi index",3,-0.5,2.5,60,-0.5,59.5);
	m_histList->Add(m_sctBSErrorMapECA[index]);
	histName=m_errorNames->at(k);
	histName+="MapECC_";
	histName+=i;
	histName+="_";
	histName+=j;
	m_sctBSErrorMapECC[index] = new TH2F(histName,"ECC; eta index; phi index",3,-0.5,2.5,60,-0.5,59.5);
	m_histList->Add(m_sctBSErrorMapECC[index]);
      }
    } 
  }
}

TList* 
SCTByteStreamErrorPlots::getHists() 
{
  return m_histList;
}

void 
SCTByteStreamErrorPlots::execute(xAOD::TEvent* thisEvent) 
{
  
  //----------------------------
  // Event information
  //--------------------------- 
  const xAOD::EventInfo* eventInfo = 0;
  if( ! thisEvent->retrieve( eventInfo, "EventInfo").isSuccess() ){
    std::cout<<"Failed to retrieve eventInfo"<<std::endl;
    return;
  }

  std::vector<int> bsErr_bec = eventInfo->auxdata<std::vector<int> >("_SCT_BSErr_bec");
  std::vector<int> bsErr_layer = eventInfo->auxdata<std::vector<int> >("_SCT_BSErr_layer");
  std::vector<int> bsErr_eta = eventInfo->auxdata<std::vector<int> >("_SCT_BSErr_eta");
  std::vector<int> bsErr_phi = eventInfo->auxdata<std::vector<int> >("_SCT_BSErr_phi");
  std::vector<int> bsErr_side = eventInfo->auxdata<std::vector<int> >("_SCT_BSErr_side");
  std::vector<int> bsErr_type = eventInfo->auxdata<std::vector<int> >("_SCT_BSErr_type");

  /*
  std::vector<int> bsErr_bec = eventInfo->auxdata<std::vector<int> >("IDDET1_SCT_BSErr_bec");
  std::vector<int> bsErr_layer = eventInfo->auxdata<std::vector<int> >("IDDET1_SCT_BSErr_layer");
  std::vector<int> bsErr_eta = eventInfo->auxdata<std::vector<int> >("IDDET1_SCT_BSErr_eta");
  std::vector<int> bsErr_phi = eventInfo->auxdata<std::vector<int> >("IDDET1_SCT_BSErr_phi");
  std::vector<int> bsErr_side = eventInfo->auxdata<std::vector<int> >("IDDET1_SCT_BSErr_side");
  std::vector<int> bsErr_type = eventInfo->auxdata<std::vector<int> >("IDDET1_SCT_BSErr_type");
  */
  int maxBarrelIndex = m_errorNames->size() * 8;
  int maxECIndex = m_errorNames->size() * 18;
  

  for (unsigned int i=0; i<bsErr_type.size(); ++i) {


    m_sctBSErrType->Fill(bsErr_type[i]);
    if (bsErr_bec[i]==0) {
      int index = 8*bsErr_type[i]+4*bsErr_side[i]+bsErr_layer[i];
      if (index<maxBarrelIndex) m_sctBSErrorMapBarrel[index]->Fill(bsErr_eta[i],bsErr_phi[i]);
    } else if (bsErr_bec[i]==-2) {      
      int index = 18*bsErr_type[i]+9*bsErr_side[i]+bsErr_layer[i];
      if (index<maxECIndex) m_sctBSErrorMapECC[index]->Fill(bsErr_eta[i],bsErr_phi[i]);
    } else if (bsErr_bec[i]==2) {
      int index = 18*bsErr_type[i]+9*bsErr_side[i]+bsErr_layer[i];
      if (index<maxECIndex) m_sctBSErrorMapECA[index]->Fill(bsErr_eta[i],bsErr_phi[i]);
    }
  }
  
  m_eventCounter++;
}

void 
SCTByteStreamErrorPlots::finalize() 
{
  
}



