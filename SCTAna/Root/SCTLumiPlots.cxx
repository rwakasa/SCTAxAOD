// (C) Claire Malone, 2015, cnbm2@cam.ac.uk
// This script produces histograms to study the feasability of using the SCT as luminometer
// The script was used for the data from 16  Dec 2012 run no. 216432 and from 3 jun 2015 run no. 266904 

#include <iostream>
#include "SCTAna/SCTLumiPlots.h"


// EDM includes: - these need to be here as if they are moved to header file the code will not compile?
#include "xAODEventInfo/EventInfo.h"
#include "xAODTruth/TruthParticleAuxContainer.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthVertex.h"
#include "xAODTracking/TrackMeasurementValidationContainer.h"
#include "xAODTracking/TrackStateValidationContainer.h"
#include "xAODTracking/TrackParticleContainer.h" //this is necessary for using xAOD::TrackParticleContainer
#include "xAODTracking/TrackParticleAuxContainer.h"

#include "xAODRootAccess/TStore.h"
#include "xAODCore/ShallowCopy.h"


 

SCTLumiPlots::SCTLumiPlots() 
{
  std::cout<<"in SCTLumiPlots constructor"<<std::endl;
  m_histList = new TList();
  m_doTimeBin = true;
  

}
void SCTLumiPlots::bookHists() // booking histograms
{
  printf("SCTLumiPlots::BookHists!!!\n");

  int bcid_bin=3564;//, bcid_bin1min=250, bcid_bin1max=450, bcid_bin2min=1180, bcid_bin2max=1320,bcid_bin3min=2080, bcid_bin3max=2200, bcid_bin4min=2970, bcid_bin4max=3080;
  //double lbn_max=0.0, lbn_min=1e10, nStrips_limit=2000, nClus_limit = 1000; 
  double mu_limit = 20;//Pixel_beam = 0.8, Pixel_noise = 0.00001, vdm = 0.0016
  //int layerInd = 0;

  //declareing histogram that plot quantities in terms of BCID
  clus_vs_BCID_barrel= new TProfile("clus_vs_BCID_barrel",";BCID; Number of Clusters in the Barrel", 200, 1700, 1900);
  m_histList->Add(clus_vs_BCID_barrel);

  clus_vs_BCID_endcap = new TProfile("clus_vs_BCID_endcap",";BCID; Number of Clusters in the Endcaps", 100, 1750, 1850);
  m_histList->Add(clus_vs_BCID_endcap);

  clus_vs_BCID = new TProfile("clus_vs_BCID",";BCID; Number of Clusters", bcid_bin, 0, bcid_bin);
  m_histList->Add(clus_vs_BCID);

  strips_vs_BCID = new TProfile("strips_vs_BCID",";BCID; Number of Strips", bcid_bin, 0, bcid_bin);
  m_histList->Add(strips_vs_BCID);

  tracks_vs_BCID = new TProfile("tracks_vs_BCID",";BCID; Number of Tracks", bcid_bin, 0, bcid_bin);
  m_histList->Add(tracks_vs_BCID);

  selectedTracks_vs_BCID = new TProfile("selectedTracks_vs_BCID",";BCID; Number of selected Tracks", bcid_bin, 0, bcid_bin);
  m_histList->Add(selectedTracks_vs_BCID);


  //this is to declare a histogram for every barrel layer of the SCT
  TString barrelHistName_mu="";
  TString barrelHistName_bcid="";
  TString barrelHistName_clusterSize="";
      for (int ib=0; ib<4; ib++) { // looping over the number  of barrel layers
	//        std::cout<<"ib="<<ib<<std::endl;
        barrelHistName_mu="clus_vs_mu_";
        barrelHistName_mu+=ib;
        barrelHistName_bcid="clus_vs_BCID_barrel_layer";
        barrelHistName_bcid+=ib;
        barrelHistName_clusterSize="clusterSizevsBCID_barrel_layer";
	barrelHistName_clusterSize+=ib;
        
        clus_vs_BCID_barrel_layer[ib] = new TProfile(barrelHistName_bcid,"barrel",bcid_bin,0,bcid_bin);
        m_histList->Add(clus_vs_BCID_barrel_layer[ib]);

        m_clusterSizevsBCID_barrel_layer[ib] = new TH2F(barrelHistName_clusterSize,"clusterSizevsBCID; BCID; cluster size",17,1780,1796,8,0,8);
        m_histList->Add(m_clusterSizevsBCID_barrel_layer[ib]);

        if (ib < 4){ // this is needed to prevent a crash 
            m_clus_vs_mu[ib] = new TProfile(barrelHistName_mu,"barrel",100,0,mu_limit);
            m_histList->Add(m_clus_vs_mu[ib]);

        }
     }
  //declaring histogramms that plot quantities in terms of mu
  strips_vs_mu = new TProfile("strips_vs_mu",";mu; Number of Strips", 50, 0, mu_limit); //100 is a sensible number of bins  
  m_histList->Add(strips_vs_mu);

  stripsmu_vs_mu = new TProfile("stripsmu_vs_mu",";mu; Number of Strips", 100, 0, mu_limit);
  m_histList->Add(stripsmu_vs_mu);

  clus_vs_mu = new TProfile("clus_vs_mu",";mu; Number of Clusters", 100, 0, mu_limit);
  m_histList->Add(clus_vs_mu);
  
  clusmu_vs_mu = new TProfile("clusmu_vs_mu",";mu; Number of Clusters", 100, 0, mu_limit); 
  m_histList->Add(clusmu_vs_mu);


  m_timeBin = new TH1F("timeBin","; time bin",8,-0.5,7.5);
  m_histList->Add(m_timeBin);
  m_timeBin_onPeak = new TH1F("timeBin_onPeak","; time bin; number of hits",8,-0.5,7.5);
  m_histList->Add(m_timeBin_onPeak);
  m_timeBin_PeakLeft = new TH1F("timeBin_PeakLeft","; time bin; number of hits",8,-0.5,7.5);
  m_histList->Add(m_timeBin_PeakLeft);
  m_timeBin_PeakRight = new TH1F("timeBin_PeakRight","; time bin; number of hits",8,-0.5,7.5);
  m_histList->Add(m_timeBin_PeakRight);
  m_timeBin_PeakAway = new TH1F("timeBin_PeakAway","; time bin; number of hits",8,-0.5,7.5);
  m_histList->Add(m_timeBin_PeakAway);
  m_timeBin_PeakRightRight = new TH1F("timeBin_PeakRightRight","; time bin; number of hits",8,-0.5,7.5);
  m_histList->Add(m_timeBin_PeakRightRight);
  m_timeBin_PeakRightRightRight = new TH1F("timeBin_PeakRightRightRight","; time bin; number of hits",8,-0.5,7.5);
  m_histList->Add(m_timeBin_PeakRightRightRight);

  //histograms of clustersize at position relative to the colliding bunch
  m_clusterSize = new TH1F("clusterSize","; clusterSize",20,-0.5,19.5);
  m_histList->Add(m_clusterSize);
  m_clusterSize_onPeak = new TH1F("clusterSize_onPeak","; cluster size; number of clusters",20,-0.5,19.5);
  m_histList->Add(m_clusterSize_onPeak);
  m_clusterSize_PeakLeft = new TH1F("clusterSize_PeakLeft","; cluster size; number of clusters",20,-0.5,19.5);
  m_histList->Add(m_clusterSize_PeakLeft );
  m_clusterSize_PeakRight = new TH1F("clusterSize_PeakRight","; cluster size; number of clusters",20,-0.5,19.5);
  m_histList->Add(m_clusterSize_PeakRight);
  m_clusterSize_PeakRightRight = new TH1F("clusterSize_PeakRightRight","; cluster size; number of clusters",20,-0.5,19.5);
  m_histList->Add(m_clusterSize_PeakRightRight);
  m_clusterSize_PeakRightRightRight = new TH1F("clusterSize_PeakRightRightRight","; cluster size; number of clusters",20,-0.5,19.5);
  m_histList->Add(m_clusterSize_PeakRightRightRight);
  m_clusterSize_PeakAway = new TH1F("clusterSize_PeakAway","; cluster size; number of clusters",20,-0.5,19.5);
  m_histList->Add(m_clusterSize_PeakAway);

  //histograms containing timeing inforation from the SCT
  m_BCIDvsTimebin = new TH2F("BCIDvsTimebin","BCIDvsTimebin; time bin; BCID",8,0,8,17,1780,1796);
  m_histList->Add(m_BCIDvsTimebin);
  m_clusterSizevsTimebin = new TH2F("clusterSizevsTimebin","clusterSizevsTimebin; time bin; cluster size",8,0,8,10,-0.5,9.5);
  m_histList->Add(m_clusterSizevsTimebin);
  m_clusterSizevsBCID = new TH2F("clusterSizevsBCID","clusterSizevsBCID; BCID; cluster size",17,1780,1796,8,0,8);
  m_histList->Add(m_clusterSizevsBCID);

  //declaring hitmaps of the whole SCT
  m_clusterLocalX = new TH1F("clusterLocalX","; local X",100,-50.,50.);
  m_histList->Add(m_clusterLocalX);

  m_sctClusterGlobalRZ = new TH2F("sctClusterGlobalRZ",";z [mm]; r [mm]",500,-3000.,3000.,500,-600.,600.);
  m_histList->Add(m_sctClusterGlobalRZ);
  
  m_sctClusterGlobalXY = new TH2F("sctClusterGlobalXY",";z [mm]; r [mm]",500,-600.,600.,500,-600.,600.);
  m_histList->Add(m_sctClusterGlobalXY);

  //hitmaps for each layer of the barrel and endcaps of the SCT
  TString histName="";
  for (int j=0; j<2;++j) {// j represents either side 0 or side 1
    for (int i=0; i<4; ++i) {// i is the layer of barrel
      histName="sctClusterMapBarrel_";
      histName+=i;
      histName+="_";
      histName+=j;
      int index=4*j+i;
      m_sctClusterMapBarrel[index] = new TH2F(histName,"barrel",13,-6.5,6.5,60,-0.5,59.5);
      m_histList->Add(m_sctClusterMapBarrel[index]);
      
    }
    for (int i=0; i<9; ++i) {// i is the layer of the endcap
      histName="sctClusterMapECA_";
      histName+=i;
      histName+="_";
      histName+=j;
      int index=9*j+i;
      m_sctClusterMapECA[index] = new TH2F(histName,"ECA",3,-0.5,2.5,60,-0.5,59.5);
      m_histList->Add(m_sctClusterMapECA[index]);
      histName="sctClusterMapECC_";
      histName+=i;
      histName+="_";
      histName+=j;
      m_sctClusterMapECC[index] = new TH2F(histName,"ECC",3,-0.5,2.5,60,-0.5,59.5);
      m_histList->Add(m_sctClusterMapECC[index]);
    }
  }

}


TList* 
SCTLumiPlots::getHists() 
{
  return m_histList;
}

void 
SCTLumiPlots::execute(xAOD::TEvent* thisEvent) // add own code here
{

//this code was taken from SCTEventloop.cxx
   const xAOD::EventInfo* eventInfo = 0;
   const xAOD::TrackParticleContainer* trackParticleContainer; //inDetTrackParticles=0;// this line was included to try to get the number of tracks 
  //std::cout<<"asdfdasfdsf"<< xAOD::InDetTrackParticles<<std::endl;

  if( ! thisEvent->retrieve( eventInfo, "EventInfo").isSuccess() )
  {
    Error("execute()", "Failed to retrieve event info collection. Exiting." );
   //return EL::StatusCode::FAILURE;
  
  }
    if (!thisEvent -> retrieve(trackParticleContainer, "InDetTrackParticles").isSuccess())
    {
     return;
    }

   

  //declaring counters that will be used to fill histograms
   int n_clusters = 0;
   int n_clusters_endcap = 0;
   int n_clusters_barrel = 0;
   int clusmu_barrel[4] = {0};
   int n_clusters_barrel_layer[4] = {0};
   //int n_tracks = 0;
   int n_strips = 0;
   double mu_max = 0.; 

 //If you don't about the track properties at all you can loop over the the SCT clusters


  const xAOD::TrackMeasurementValidationContainer* sctClusters = 0;
  if ( !thisEvent->retrieve( sctClusters, "SCT_Clusters" ).isSuccess() ){ // retrieve arguments: container type, container key
    Error("execute()", "Failed to retrieve SCT Clusters. Exiting." );
    return;//// EL::StatusCode::FAILURE;
  }

  //std::cout<<"mu = "<<eventInfo->actualInteractionsPerCrossing()<<std::endl;
  ///  Info("execute()",  "Looping over the PRD container");
  if (sctClusters->size()==0){return;} //only continued if there are stable beams
  for( xAOD::TrackMeasurementValidationContainer::const_iterator cluster_itr = sctClusters->begin(); cluster_itr != sctClusters->end(); cluster_itr++) // beginning of cluster loop

  {
    


    // we can make integers out of vectors of int
    int bec = (*cluster_itr)->auxdataConst<int>("bec");
    int layer = (*cluster_itr)->auxdataConst<int>("layer");
    int side = (*cluster_itr)->auxdataConst<int>("side");
    int eta_module = (*cluster_itr)->auxdataConst<int>("eta_module");
    int phi_module = (*cluster_itr)->auxdataConst<int>("phi_module");
    n_clusters+=1;// adding up the number of clusterns in event
    
    tracks_vs_BCID->Fill(eventInfo->bcid(), trackParticleContainer->size());



    if ((bec==0) && (layer < 4)) { // this for the barrel
	int index = 4*side+layer; 
//         std::cout<<"side = "<<side<<" layer = "<<layer<<" index = "<<index<<std::endl;
	m_sctClusterMapBarrel[index]->Fill(eta_module,phi_module);	
        n_clusters_barrel += 1;
        n_clusters_barrel_layer[layer] += 1;
        clusmu_barrel[layer] += 1; //there should not even be an array to fill for a layer > 3       
    }
   // std::cout<<"layer = "<<layer<<"clusmu_barrel[layer] = "<<clusmu_barrel[layer]<<std::endl;
    
    if ((bec==-2) && (layer < 9)) { // this is for endcapC
	int index = 9*side+layer;
	m_sctClusterMapECC[index]->Fill(eta_module,phi_module);
    } 
    if ((bec==-2) || (bec == 2)) { // this is for both endcaps
    n_clusters_endcap += 1;
    }

    else if ((bec==2) && (layer < 9)) { // this is for endcapA
	int index = 9*side+layer;
	m_sctClusterMapECA[index]->Fill(eta_module,phi_module);
    }

    // this is to plot the position of the clusters
    float globalX= (*cluster_itr)->globalX(); //it uses cluster_itr which is introduced in the for loop over the xAOD to get the position of the clusters
    float globalY= (*cluster_itr)->globalY();
    float globalZ= (*cluster_itr)->globalZ();
    float globalR=pow(globalX*globalX+globalY*globalY,0.5);

    
    m_clusterLocalX->Fill((*cluster_itr)->localX());
    if (globalY<0) globalR=-1.*globalR;
    m_sctClusterGlobalRZ->Fill(globalZ, globalR);
    if (fabs(globalY)<500.)
      m_sctClusterGlobalXY->Fill(globalX, globalY);
    if ( (*cluster_itr)->isAvailable< int  >( "SiWidth" ) ) {
      m_clusterSize->Fill((*cluster_itr)->auxdataConst<int>("SiWidth"));
        if ((bec==0) && (layer < 4)) { // this for the barrel 
            m_clusterSizevsBCID_barrel_layer[layer]->Fill(eventInfo->bcid(),(*cluster_itr)->auxdataConst<int>("SiWidth"));
        }

    }
    // this is to obtain time bin information
    if ( m_doTimeBin && (*cluster_itr)->isAvailable< std::vector<int>  >( "rdo_timebin" ) ) {
	std::vector<int> rdoTimebin = (*cluster_itr)->auxdataConst< std::vector<int>  >( "rdo_timebin" );
	std::vector<int>::iterator rdoIter=rdoTimebin.begin();
	for (; rdoIter!=rdoTimebin.end(); ++rdoIter) {
	  m_timeBin->Fill((*rdoIter));
          //assuming the peak is at a BCID of 1786
          m_BCIDvsTimebin->Fill(*rdoIter,eventInfo->bcid());
          m_clusterSizevsTimebin->Fill(*rdoIter,(*cluster_itr)->auxdataConst<int>("SiWidth"));
          m_clusterSizevsBCID->Fill(eventInfo->bcid(),(*cluster_itr)->auxdataConst<int>("SiWidth"));
        if (*rdoIter == 2 || *rdoIter == 3){//we add the cluster size to the counter for the number of strips if the time bin 010 or 011 is activated (in binary 010 = 2 and 011 = 3 )
            n_strips+=sctClusters->size();
        }
          // the values of the BCID used here to study the timeing information were chosen for the VdM  stream for the afterglow run 2266904.
          // there were two colliding bunch pairs and the one studied here has a BCID of 1786
          if (eventInfo->bcid() > 2100 && eventInfo->bcid()<2200 ) // away from the colliding bunch pair
            { 
                m_timeBin_PeakAway -> Fill((*rdoIter));
                m_clusterSize_PeakAway ->Fill((*cluster_itr)->auxdataConst<int>("SiWidth"));
            }
            if (eventInfo->bcid() == 1786)
            { 
                m_timeBin_onPeak -> Fill((*rdoIter));
                m_clusterSize_onPeak ->Fill((*cluster_itr)->auxdataConst<int>("SiWidth"));
            }
            if (eventInfo->bcid() == 1785) // prior BCID to 1786, to study out of time hits
            { 
                m_timeBin_PeakLeft -> Fill((*rdoIter));
                m_clusterSize_PeakLeft ->Fill((*cluster_itr)->auxdataConst<int>("SiWidth"));
            }
            if (eventInfo->bcid() > 1786&& eventInfo->bcid()<1793) // post BCIDs to 1786, to study out of time hits
            { 
                m_timeBin_PeakRight -> Fill((*rdoIter));
                m_clusterSize_PeakRight ->Fill((*cluster_itr)->auxdataConst<int>("SiWidth"));
            }
            if (eventInfo->bcid() > 1788&& eventInfo->bcid() < 1793) // dto.
            {

                m_timeBin_PeakRightRight -> Fill((*rdoIter));
                m_clusterSize_PeakRightRight ->Fill((*cluster_itr)->auxdataConst<int>("SiWidth"));
            } 
           if (eventInfo->bcid() > 1789&& eventInfo->bcid() < 1793) //dto.
            {

                m_timeBin_PeakRightRightRight -> Fill((*rdoIter));
                m_clusterSize_PeakRightRightRight ->Fill((*cluster_itr)->auxdataConst<int>("SiWidth"));
            }
// 	  std::cout<<" timebin is "<<(*rdoIter)<<std::endl;
	}

	std::vector<int> rdoStrip = (*cluster_itr)->auxdataConst< std::vector<int>  >( "rdo_strip" );
	std::vector<int>::iterator rdoIterStrip=rdoStrip.begin();
	for (; rdoIterStrip!=rdoStrip.end(); ++rdoIterStrip) {
	  //	  m_timeBin->Fill((*rdoIter));
	  /// std::cout<<" strip is "<<(*rdoIterStrip)<<std::endl;
	}
	
    }

    strips_vs_BCID->Fill(eventInfo->bcid(), n_strips);
    
  } // end of loop over clusters  

    clus_vs_BCID_barrel->Fill(eventInfo->bcid(),n_clusters_barrel);
    clus_vs_BCID_endcap->Fill(eventInfo->bcid(),n_clusters_endcap);
    
    clus_vs_mu->Fill(eventInfo->actualInteractionsPerCrossing(),n_clusters);
    clus_vs_BCID->Fill(eventInfo->bcid(),n_clusters);
    
    for (int layer = 0; layer < 4; layer++)
    {
//         clus_vs_mu[layer].Fill(eventInfo->actualInteractionsPerCrossing(),clusmu_barrel[layer]);
        clus_vs_BCID_barrel_layer[layer]->Fill(eventInfo->bcid(), n_clusters_barrel_layer[layer]); 
    }

    strips_vs_mu->Fill(eventInfo->actualInteractionsPerCrossing(),n_strips);
    if ( eventInfo->actualInteractionsPerCrossing()>0 )
    {
    double x = eventInfo->actualInteractionsPerCrossing();
    //stripsmu_vs_mu->Fill(eventInfo->actualInteractionsPerCrossing(),n_strips/eventInfo->actualInteractionsPerCrossing());
    stripsmu_vs_mu->Fill(x,n_strips/ x);

    clusmu_vs_mu->Fill(eventInfo->actualInteractionsPerCrossing(),n_clusters/eventInfo->actualInteractionsPerCrossing());
    }

// find max of mu
if (mu_max < eventInfo->actualInteractionsPerCrossing()) {
    mu_max = eventInfo->actualInteractionsPerCrossing();
    }

}

void 
SCTLumiPlots::finalize() 
{
     printf("Finalize!!!\n");

}



