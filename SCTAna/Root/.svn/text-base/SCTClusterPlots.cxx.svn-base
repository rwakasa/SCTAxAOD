#include <iostream>
#include "SCTAna/SCTClusterPlots.h"

#include "SCTAna/SCTModuleCoordMap.h"

// EDM includes: - if move to header file will not compile?
#include "xAODEventInfo/EventInfo.h"
#include "xAODTruth/TruthParticleAuxContainer.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthVertex.h"
#include "xAODTracking/TrackMeasurementValidationContainer.h"
#include "xAODTracking/TrackStateValidationContainer.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTracking/TrackParticleAuxContainer.h"

#include "xAODRootAccess/TStore.h"
#include "xAODCore/ShallowCopy.h"

SCTClusterPlots::SCTClusterPlots() 
{
  std::cout<<"in SCTClusterPlots constructor"<<std::endl;
  m_histList = new TList();
  m_doTimeBin = true;
  std::string RootCoreBinDir = std::getenv("ROOTCOREBIN");

  m_sctMapping = getSCTMapping(RootCoreBinDir+"/data/SCTAna/cabling_Mar2015.dat");
  
}
void SCTClusterPlots::bookHists() 
{
  m_sctClusterGlobalRZ = new TH2F("sctClusterGlobalRZ",";z [mm]; r [mm]",500,-3000.,3000.,500,-600.,600.);
  m_histList->Add(m_sctClusterGlobalRZ);
  
  m_sctClusterGlobalXY = new TH2F("sctClusterGlobalXY",";z [mm]; r [mm]",500,-600.,600.,500,-600.,600.);
  m_histList->Add(m_sctClusterGlobalXY);

  m_timeBin = new TH1F("timeBin","; time bin",8,-0.5,7.5);
  m_histList->Add(m_timeBin);

  m_clusterSize = new TH1F("clusterSize","; clusterSize",20,-0.5,19.5);
  m_histList->Add(m_clusterSize);

  m_clusterLocalX = new TH1F("clusterLocalX","; local X",100,-50.,50.);
  m_histList->Add(m_clusterLocalX);

  m_sctClusterHashChip = new TH2I("sctClusterHashChip", "sctClusterHashChip;hash;chip", 8176/2, -0.5, 8175.5, 12, -0.5, 11.5);
  m_histList->Add(m_sctClusterHashChip);

  TString histName="";
  for (int j=0; j<2;++j) {
    for (int i=0; i<4; ++i) {
      histName="sctClusterMapBarrel_";
      histName+=i;
      histName+="_";
      histName+=j;
      int index=4*j+i;
      m_sctClusterMapBarrel[index] = new TH2F(histName,"barrel",13,-6.5,6.5,60,-0.5,59.5);
      m_histList->Add(m_sctClusterMapBarrel[index]);
      
    }
    for (int i=0; i<9; ++i) {
      histName="sctClusterMapECA_";
      histName+=i;
      histName+="_";
      histName+=j;
      int index=9*j+i;
      m_sctClusterMapECA[index] = new TH2F(histName,"ECA",3,-0.5,2.5,60,-0.5,59.5);
      m_histList->Add(m_sctClusterMapECA[index]);
      histName="sctClusterMapECC_";
      histName+=i;
      histName+="_";
      histName+=j;
      m_sctClusterMapECC[index] = new TH2F(histName,"ECC",3,-0.5,2.5,60,-0.5,59.5);
      m_histList->Add(m_sctClusterMapECC[index]);
    }
  }
}

TList* 
SCTClusterPlots::getHists() 
{
  return m_histList;
}

void 
SCTClusterPlots::execute(xAOD::TEvent* thisEvent) 
{
   
  //If you don't about the track properties at all you can loop over the the SCT clusters

  
  const xAOD::TrackMeasurementValidationContainer* sctClusters = 0;
  if ( !thisEvent->retrieve( sctClusters, "SCT_Clusters" ).isSuccess() ){ // retrieve arguments: container type, container key
    Error("execute()", "Failed to retrieve SCT Clusters. Exiting." );
    return;//// EL::StatusCode::FAILURE;
  }
 
  ///  std::cout<<" number of SCT clusters is "<<sctClusters->size()<<std::endl;
  
 
  ///  Info("execute()",  "Looping over the PRD container");
  for( xAOD::TrackMeasurementValidationContainer::const_iterator cluster_itr = sctClusters->begin();
       cluster_itr != sctClusters->end(); cluster_itr++) 
  {  

    int bec = (*cluster_itr)->auxdataConst<int>("bec");
    int layer = (*cluster_itr)->auxdataConst<int>("layer");
    int eta_module = (*cluster_itr)->auxdataConst<int>("eta_module");
    int phi_module = (*cluster_itr)->auxdataConst<int>("phi_module");
    int side = (*cluster_itr)->auxdataConst<int>("side");

    //// EXAMPLE OF HOW TO USE MAPPING CODE:

    std::string ident = std::to_string(bec);
    ident+="_";
    ident+=std::to_string(layer);
    ident+="_";
    ident+=std::to_string(phi_module);    
    ident+="_";
    ident+=std::to_string(eta_module);    
    //    std::cout<<"ident is "<<ident<<std::endl;
    //std::cout<<" length of map is "<<m_sctMapping.size()<<std::endl;
    
    //    std::cout << "hash: for ident " <<ident<<" is "<< (m_sctMapping[ident] ? m_sctMapping[ident]->hash() : -1) << std::endl;
    int hash = (m_sctMapping[ident] ? m_sctMapping[ident]->hash() : -1);

    /// END OF EXAMPLE


    if ((bec==0) && (layer < 4)) {
	int index = 4*side+layer;
	m_sctClusterMapBarrel[index]->Fill(eta_module,phi_module);	
    } else if ((bec==-2) && (layer < 9)) {
	int index = 9*side+layer;
	m_sctClusterMapECC[index]->Fill(eta_module,phi_module);
    } else if ((bec==2) && (layer < 9)) {
	int index = 9*side+layer;
	m_sctClusterMapECA[index]->Fill(eta_module,phi_module);
    }

    float globalX= (*cluster_itr)->globalX();
    float globalY= (*cluster_itr)->globalY();
    float globalZ= (*cluster_itr)->globalZ();
    float globalR=pow(globalX*globalX+globalY*globalY,0.5);
    m_clusterLocalX->Fill((*cluster_itr)->localX());
    if (globalY<0) globalR=-1.*globalR;
    ////if ((bec==2) && (layer==8))  std::cout<<" ec layer 8 side "<<side<<" z-position "<<globalZ<<std::endl;
    
    m_sctClusterGlobalRZ->Fill(globalZ, globalR);
    if (fabs(globalY)<500.)
      m_sctClusterGlobalXY->Fill(globalX, globalY);

    if ( (*cluster_itr)->isAvailable< int  >( "SiWidth" ) ) {
      m_clusterSize->Fill((*cluster_itr)->auxdataConst<int>("SiWidth"));
      //      std::cout << "SiWidth: " << (*cluster_itr)->auxdataConst<int>("SiWidth") << std::endl;
    }
    if ( m_doTimeBin && (*cluster_itr)->isAvailable< std::vector<int>  >( "rdo_timebin" ) ) {
	std::vector<int> rdoTimebin = (*cluster_itr)->auxdataConst< std::vector<int>  >( "rdo_timebin" );
	std::vector<int>::iterator rdoIter=rdoTimebin.begin();
	for (; rdoIter!=rdoTimebin.end(); ++rdoIter) {
	  m_timeBin->Fill((*rdoIter));
	}

	std::vector<int> rdoStrip = (*cluster_itr)->auxdataConst< std::vector<int>  >( "rdo_strip" );
	std::vector<int>::iterator rdoIterStrip=rdoStrip.begin();
	for (; rdoIterStrip!=rdoStrip.end(); ++rdoIterStrip) {
	  //	  m_timeBin->Fill((*rdoIter));
	  int mychip = (*rdoIterStrip)/128; // strip is from 0 to 767

	  bool reversed = false;
	  if(bec==-2 or bec==+2) { // Some regions of Endcap use reversed chip numbering
	    if(eta_module==2) reversed = true; // Inner rings
	    if(layer==8) reversed = true; // Disk 9
	    if(bec==-2) reversed = (not reversed); // Endcap C
	  }
	  if(reversed) mychip = 5-mychip; // chip 0 -> 5, 1 -> 4, ..., 5 -> 0
	  if(side==1) mychip = 11-mychip; // side 1 has chips 6 to 11

	  m_sctClusterHashChip->Fill(hash, mychip);
	}
	
    } 
  }  
   
}

void 
SCTClusterPlots::finalize() 
{
  
}



