#include <iostream>
#include "SCTAna/SCTDataSize.h"
#include "xAODEventInfo/EventInfo.h"

#include "xAODRootAccess/TStore.h"

#include "xAODTracking/SCTRawHitValidation.h"
#include "xAODTracking/SCTRawHitValidationContainer.h"
#include "xAODTracking/SCTRawHitValidationAuxContainer.h"

#include <vector>
#include <map>
#include <algorithm>

SCTDataSize::SCTDataSize() 
{
  std::cout<<"in SCTDataSize constructor"<<std::endl;
  m_histList = new TList();
  std::string RootCoreBinDir = std::getenv("ROOTCOREBIN");
  m_isFirstEvent=true;
}

void SCTDataSize::bookHists() 
{
  m_h_mu = new TH1I("h_mu", "h_mu", 1000, 0., 100.);
  m_histList->Add(m_h_mu);
  m_h_ABCD = new TH1I("h_ABCD", "h_ABCD", 3*9*13*60*2*6+1, -0.5, 3*9*13*60*2*6+0.5);
  m_histList->Add(m_h_ABCD);    
  m_h_muLink = new TH2I("h_muLink", "h_muLink", 1000, 0., 100., 3*9*13*60*2+1, -0.5, 3*9*13*60*2+0.5);
  m_histList->Add(m_h_muLink);    
  m_h_muABCD = new TH2I("h_muABCD", "h_muABCD", 1000, 0., 100., 3*9*13*60*2+1, -0.5, 3*9*13*60*2+0.5);
  m_histList->Add(m_h_muABCD);
  m_h_muLinkBit = new TH2I("h_muLinkBit", "h_muLinkBit", 1000, 0., 100., 3*9*13*60*2+1, -0.5, 3*9*13*60*2+0.5);
  m_histList->Add(m_h_muLinkBit);    
  m_h_muLinkExpanded = new TH2I("h_muLinkExpanded", "h_muLinkExpanded", 1000, 0., 100., 3*9*13*60*2+1, -0.5, 3*9*13*60*2+0.5);
  m_histList->Add(m_h_muLinkExpanded);    
  m_h_muLinkCondensed = new TH2I("h_muLinkCondensed", "h_muLinkCondensed", 1000, 0., 100., 3*9*13*60*2+1, -0.5, 3*9*13*60*2+0.5);
  m_histList->Add(m_h_muLinkCondensed);    
  m_h_muLinkSuperCondensed = new TH2I("h_muLinkSuperCondensed", "h_muLinkSuperCondensed", 1000, 0., 100., 3*9*13*60*2+1, -0.5, 3*9*13*60*2+0.5);
  m_histList->Add(m_h_muLinkSuperCondensed);    
}

TList* 
SCTDataSize::getHists() 
{
  return m_histList;
}

void 
SCTDataSize::execute(xAOD::TEvent* thisEvent) 
{

  //----------------------------
  // Event information
  //--------------------------- 

  // Initialize on the first event
  m_isFirstEvent = false;
  if (m_isFirstEvent) {
    m_isFirstEvent=false;
  }

  const xAOD::EventInfo* eventInfo = 0;
  if( ! thisEvent->retrieve( eventInfo, "EventInfo").isSuccess() ){
    std::cout<<"Failed to retrieve eventInfo"<<std::endl;
    return;
  }

  //  int lumiBlock = eventInfo->lumiBlock();

  float mu = eventInfo->auxdata<float>("actualInteractionsPerCrossing");
  m_h_mu->Fill(mu);

  const xAOD::SCTRawHitValidationContainer* rdos = 0;
  if(!thisEvent->retrieve(rdos, "SCT_RawHits").isSuccess()) {
    Error("execute()", "Failed to retrieve SCT RDOs. Exiting." );
    return;
  }

  unsigned int nABCDs = 0; // The number of fired ABCD chips in this event
  std::map<int, unsigned int> sctRdo_idMap; // id to numbering only for this event
  std::vector<std::vector<int> > sctRdo_stripsInABCDs; // List of strips in individual chips 

  for(xAOD::SCTRawHitValidationContainer::const_iterator rdo_itr = rdos->begin(); rdo_itr != rdos->end(); rdo_itr++) {
    const xAOD::SCTRawHitValidation *rdo = *rdo_itr;
  
    int timeBin = rdo->getTimeBin();
    if(!(timeBin & 0x2) || (timeBin & 0x4)) continue; // Require 01X hit pattern 
  
    int bec = rdo->auxdataConst<int>("bec");
    int layer = rdo->auxdataConst<int>("layer");
    int eta_module = rdo->auxdataConst<int>("eta_module");
    int phi_module = rdo->auxdataConst<int>("phi_module");
    int side = rdo->auxdataConst<int>("side");
    int strip = rdo->getStrip();
    int chip = getChip(bec, layer, eta_module, side, strip);
    if(isDisabledChip(bec,layer, eta_module, phi_module, side, chip)) continue;

    // General numbering 
    int ABCDid = ((((((bec+2)/2 /* bec: -2, 0, +2 */)*9
		     + layer /* layer: 0 -- +8 */)*13
		    + eta_module+6 /* eta: -6 -- +6 */)*60
		   + phi_module /* phi: 0 -- 57 */)*2
		  + side /* side: 0, +1 */)*6
      + strip/128; // 0-5 to identify chips 

    std::pair<std::map<int, unsigned int>::iterator, bool> result;
    result = sctRdo_idMap.insert(std::pair<int, unsigned int>(ABCDid, nABCDs)); 
    if(result.second) { // If the ABCDid is new one, a new vector is created to hold strips  
      std::vector<int> sctRdo_stripsInABCD;
      sctRdo_stripsInABCDs.push_back(sctRdo_stripsInABCD);
      nABCDs++;
    }
    
    int groupSize = rdo->getGroupSize();
    if(groupSize==0) {
      Warning("execute()", "groupSize is zero");
    }

    unsigned int iABCD = (*(result.first)).second; // Get numbering only for this event  
    for(int iStrip=0; iStrip<groupSize; iStrip++) { // Fill strips with the strip size. The size is zero if expanded mode.  
      sctRdo_stripsInABCDs.at(iABCD).push_back(strip+iStrip); 
    }

  }

  // Clustering for all fired ABCDs
  unsigned int nClustersInEvent = 0;
  unsigned int nStripsInEvent = 0;
  std::vector<int> linkIds;
  std::map<int, unsigned int>::iterator it   = sctRdo_idMap.begin();
  std::map<int, unsigned int>::iterator it_e = sctRdo_idMap.end();
  for(; it!=it_e; it++) {
    int ABCDid = it->first; // General numbering
    m_h_ABCD->Fill(ABCDid);
    int linkId = ABCDid/6; // To combine 6 chips to one link

    unsigned int iABCD = it->second;
    std::vector<int> sctRdo_stripsInABCD = sctRdo_stripsInABCDs.at(iABCD);
    // Sort strips
    std::sort(sctRdo_stripsInABCD.begin(), sctRdo_stripsInABCD.end());
    // Make clusters
    unsigned int nClustersInABCD = 0;
    int previousStrip = -999;
    unsigned int clusterSize = 0;
    unsigned int nStripsInABCD = sctRdo_stripsInABCD.size(); // The number of strips in this ABCD chip
    //    m_h_nStripsInABCD->Fill(nStripsInABCD);
    nStripsInEvent += nStripsInABCD;
    for(unsigned int iStrip=0; iStrip<nStripsInABCD; iStrip++) { // Loop over all strips in this ABCD chip
      if((iStrip!=0) && (sctRdo_stripsInABCD.at(iStrip)!=previousStrip+1)) { // If this strip is not the adjacent one with the previous strip, a cluster is made.  
	m_h_muLinkBit->Fill(mu, linkId, 13+clusterSize*4);
	m_h_muLinkExpanded->Fill(mu, linkId, (clusterSize/2+1));
	m_h_muLinkCondensed->Fill(mu, linkId, ((clusterSize+1)/2));
	m_h_muLinkSuperCondensed->Fill(mu, linkId, ((clusterSize-1)/16+1));

	clusterSize = 0;
	nClustersInABCD++;
	nClustersInEvent++;
      }

      previousStrip = sctRdo_stripsInABCD.at(iStrip);
      clusterSize++;
    }
    // Open cluster is the last cluster in this chip 
    m_h_muLinkBit->Fill(mu, linkId, 13+clusterSize*4);
    m_h_muLinkExpanded->Fill(mu, linkId, (clusterSize/2+1));
    m_h_muLinkCondensed->Fill(mu, linkId, ((clusterSize+1)/2));
    m_h_muLinkSuperCondensed->Fill(mu, linkId, ((clusterSize-1)/16+1));
    nClustersInABCD++;
    nClustersInEvent++;

    // Count the number of links with hits 
    if(std::find(linkIds.begin(), linkIds.end(), linkId)==linkIds.end()) {
      // If the link is new one, add it to the vector.  
      linkIds.push_back(linkId);
      m_h_muLink->Fill(mu, linkId);
      for(int ABCDid=linkId*6; ABCDid<linkId*6+6; ABCDid++) {
	m_h_muABCD->Fill(mu, linkId);
      }
    }
  }

}

void 
SCTDataSize::finalize() 
{
}

bool SCTDataSize::isDisabledChip(int bec, int layer, int eta_module, int phi_module, int side, int chip) {
  return false;
}

int SCTDataSize::getChip(int bec, int layer, int eta_module, int side, int strip) {
  bool swapped = false;
  if(bec==-2 or bec==+2) { // Some regions of Endcap use swapped chip numbering
    if(eta_module==2) swapped = true; // Inner rings
    if(layer==8) swapped = true; // Disk 9
    if(bec==-2) swapped = (not swapped); // Endcap C
  }
  int chip = strip/128; // strip is from 0 to 767
  if(swapped) chip = 5-chip; // chip 0 -> 5, 1 -> 4, ..., 5 -> 0
  if(side==1) chip = 11-chip; // side 1 has chips 6 to 11
  
  return chip;
}
