#include <iostream>
#include "SCTAna/SCTClusterOnTrackPlots.h"


// EDM includes: - if move to header file will not compile?
#include "xAODEventInfo/EventInfo.h"
#include "xAODTruth/TruthParticleAuxContainer.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthVertex.h"
#include "xAODTracking/TrackMeasurementValidationContainer.h"
#include "xAODTracking/TrackStateValidationContainer.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTracking/TrackParticleAuxContainer.h"

#include "xAODRootAccess/TStore.h"
#include "xAODCore/ShallowCopy.h"

SCTClusterOnTrackPlots::SCTClusterOnTrackPlots() 
{
  std::cout<<"in SCTClusterOnTrackPlots constructor"<<std::endl;
  m_histList = new TList();
  m_doTimeBin = false;
  
}
void SCTClusterOnTrackPlots::bookHists() 
{
  m_sctClusterGlobalRZTrack = new TH2F("sctClusterGlobalRZTrack",";z [mm]; r [mm]",500,-3000.,3000.,500,-600.,600.);
  m_histList->Add(m_sctClusterGlobalRZTrack);
  
  m_sctClusterGlobalXYTrack = new TH2F("sctClusterGlobalXYTrack",";z [mm]; r [mm]",500,-600.,600.,500,-600.,600.);
  m_histList->Add(m_sctClusterGlobalXYTrack);
 
  m_msosDetType = new TH1F("msosDetType","; DetType; Number of MSOS",4,0.,4.);
  m_histList->Add(m_msosDetType);

  m_nSCTHitsOnTrack = new TH1F("nSCTHitsOnTrack", "; Number of SCT hits", 20,-0.5,19.5);
  m_histList->Add(m_nSCTHitsOnTrack);
  
  m_sctClusOnTrackTimeBin = new TH1F("sctClusOnTrackTimeBin","; time bin",8,-0.5,7.5);
  m_histList->Add(m_sctClusOnTrackTimeBin);

  m_sctClusOnTrackSize = new TH1F("sctClusOnTrackSize","; clusterSize",20,-0.5,19.5);
  m_histList->Add(m_sctClusOnTrackSize);


  m_sctClusLocalPhi = new TH1F("sctClusLocalPhi","; local phi", 100,-1.6,1.6); 
  m_histList->Add(m_sctClusLocalPhi);

  m_sctClusLocalTheta = new TH1F("sctClusLocalTheta","; local theta", 100,0.,3.2); 
  m_histList->Add(m_sctClusLocalTheta);

  m_sctClusUnbiasedResidualX = new TH1F("sctClusUnbiasedResidualX","; unbiased residual X", 100,-0.5,0.5); 
  m_histList->Add(m_sctClusUnbiasedResidualX);

  m_sctClusOnTrackLocalX = new TH1F("sctClusOnTrackLocalX","; local x", 100,-50.,50.); 
  m_histList->Add(m_sctClusOnTrackLocalX);


  m_sctClusSizeVsLocalPhi = new TProfile("sctClusSizeVsLocalPhi","; local phi; cluster size",100,-1.6,1.6);
  m_histList->Add(m_sctClusSizeVsLocalPhi);

  m_nPixVsSCTHitsOnTrack = new TH2F("nPixVsSCTHitsOnTrack", "; Number of Pixel hits; Number of SCT hits", 10,-0.5,9.5,20,-0.5,19.5);
  m_histList->Add(m_nPixVsSCTHitsOnTrack);

  m_nSCTHitsOnTrackVsEta = new TProfile("nSCTHitsOnTrackVsEta","; Track #eta; <nSCTHits>",100,-2.5,2.5);
  m_histList->Add(m_nSCTHitsOnTrackVsEta);

  m_nSCTHolesOnTrackVsEta = new TProfile("nSCTHolesOnTrackVsEta","; Track #eta; <nSCTHoles>",100,-2.5,2.5);
  m_histList->Add(m_nSCTHolesOnTrackVsEta);
  
  m_trackPT = new TH1F("trackPT","; Track p_{T} [GeV]; Number of tracks",100,0.,50.);
  m_histList->Add(m_trackPT);
  m_trackEta = new TH1F("trackEta","; Track #eta; Number of tracks",100,-2.5,2.5);
  m_histList->Add(m_trackEta);
  m_trackPhi = new TH1F("trackPhi","; Track #phi; Number of tracks",100,-3.15,3.15);
  m_histList->Add(m_trackPhi);
  m_trackD0 = new TH1F("trackD0","; Track d_{0} [mm]; Number of tracks",100,-10,10);
  m_histList->Add(m_trackD0);

  m_trackPT_withSCT = new TH1F("trackPT_withSCT","; Track p_{T} [GeV]; Number of tracks",100,0.,50.);
  m_histList->Add(m_trackPT_withSCT);
  m_trackEta_withSCT = new TH1F("trackEta_withSCT","; Track #eta; Number of tracks",100,-2.5,2.5);
  m_histList->Add(m_trackEta_withSCT);
  m_trackPhi_withSCT = new TH1F("trackPhi_withSCT","; Track #phi; Number of tracks",100,-3.15,3.15);
  m_histList->Add(m_trackPhi_withSCT);
  m_trackD0_withSCT = new TH1F("trackD0_withSCT","; Track d_{0} [mm]; Number of tracks",100,-10,10);
  m_histList->Add(m_trackD0_withSCT);

  TString histName="";
  for (int j=0; j<2;++j) {
    for (int i=0; i<4; ++i) {
      histName="sctClusterOnTrackMapBarrel_";
      histName+=i;
      histName+="_";
      histName+=j;
      int index = 4*j+i;
      m_sctClusterOnTrackMapBarrel[index] = new TH2F(histName,"barrel; eta index; phi index",13,-6.5,6.5,60,-0.5,59.5);
      m_histList->Add(m_sctClusterOnTrackMapBarrel[index]);
      
    }
    for (int i=0; i<9; ++i) {
      histName="sctClusterOnTrackMapECA_";
      histName+=i;
      histName+="_";
      histName+=j;
      int index = 9*j+i;
      m_sctClusterOnTrackMapECA[index] = new TH2F(histName,"ECA; eta index; phi index",3,-0.5,2.5,60,-0.5,59.5);
      m_histList->Add(m_sctClusterOnTrackMapECA[index]);
      histName="sctClusterOnTrackMapECC_";
      histName+=i;
      histName+="_";
      histName+=j;
      m_sctClusterOnTrackMapECC[index] = new TH2F(histName,"ECC; eta index; phi index",3,-0.5,2.5,60,-0.5,59.5);
      m_histList->Add(m_sctClusterOnTrackMapECC[index]);
    }
  }

}

TList* 
SCTClusterOnTrackPlots::getHists() 
{
  return m_histList;
}


void 
SCTClusterOnTrackPlots::execute(xAOD::TEvent* thisEvent) 
{
  
  // get track container of interest
  const xAOD::TrackParticleContainer* recoTracks = 0;
  if ( !thisEvent->retrieve( recoTracks, "InDetTrackParticles" ).isSuccess() ){ // retrieve arguments: container type, container key
    Error("execute()", "Failed to retrieve Reconstructed Track container. Exiting." );
    return;//// EL::StatusCode::FAILURE;
  }
  
  // loop over the tracks in the container
  for( xAOD::TrackParticleContainer::const_iterator recoTrk_itr = recoTracks->begin();
          recoTrk_itr != recoTracks->end(); recoTrk_itr++) {
    int nClusOnTrack=0;
    
    // Make your track selection
    /*    if ( (*recoTrk_itr)->pt() < 1000 )
      continue;

        Info("execute()",  "Track  (pT, eta, phi)  = (%f, %f, %f)", (*recoTrk_itr)->pt(), (*recoTrk_itr)->eta(), (*recoTrk_itr)->phi() );
    */
    
    int nPixHits=(int)((*recoTrk_itr)->auxdata<uint8_t>("numberOfPixelHits"));


    int nSCTHits=(int)((*recoTrk_itr)->auxdata<uint8_t>("numberOfSCTHits"));
    int nSCTHoles=(int)((*recoTrk_itr)->auxdata<uint8_t>("numberOfSCTHoles"));
    m_nSCTHitsOnTrack->Fill(nSCTHits);
    
    m_nPixVsSCTHitsOnTrack->Fill(nPixHits,nSCTHits);


    m_nSCTHitsOnTrackVsEta->Fill((*recoTrk_itr)->eta(),nSCTHits);
    m_nSCTHolesOnTrackVsEta->Fill((*recoTrk_itr)->eta(),nSCTHoles);
    
    m_trackPT->Fill((*recoTrk_itr)->pt()/1000.);
    m_trackEta->Fill((*recoTrk_itr)->eta());    
    m_trackPhi->Fill((*recoTrk_itr)->phi());
    m_trackD0->Fill((*recoTrk_itr)->d0());

    if (nSCTHits<4) continue;
    
    m_trackPT_withSCT->Fill((*recoTrk_itr)->pt()/1000.);
    m_trackEta_withSCT->Fill((*recoTrk_itr)->eta());    
    m_trackPhi_withSCT->Fill((*recoTrk_itr)->phi());
    m_trackD0_withSCT->Fill((*recoTrk_itr)->d0());
    

    typedef std::vector<ElementLink< xAOD::TrackStateValidationContainer > > MeasurementsOnTrack;
    typedef std::vector<ElementLink< xAOD::TrackStateValidationContainer > >::const_iterator MeasurementsOnTrackIter;
    static const char* measurementNames = "msosLink";   //Note the prefix could change
    //    static const char* measurementNames = "IDDET1_msosLink";   //Note the prefix could change


    // Check if there are MSOS attached
    if( ! (*recoTrk_itr)->isAvailable< MeasurementsOnTrack >( measurementNames ) ) {
      std::cout<<" Measurements on track not available"<<std::endl;
      continue;
    }
    // Get the MSOS's
    const MeasurementsOnTrack& measurementsOnTrack = (*recoTrk_itr)->auxdata< MeasurementsOnTrack >( measurementNames );

    // Loop over track TrackStateValidation's
    
    for( MeasurementsOnTrackIter msos_iter = measurementsOnTrack.begin();
	 msos_iter != measurementsOnTrack.end(); ++msos_iter) {  
      //Check if the element link is valid
      if( ! (*msos_iter).isValid() ) {
	continue;
      }
      
      const xAOD::TrackStateValidation* msos = *(*msos_iter); 
      m_msosDetType->Fill(msos->detType()); /// 1 is pixel, 2 is SCT

      if( (int)(msos->detType()) != 2 ) continue;
      
      //Get SCT cluster
      if(  msos->trackMeasurementValidationLink().isValid() && *(msos->trackMeasurementValidationLink()) ){

	const xAOD::TrackMeasurementValidation* sctCluster =  *(msos->trackMeasurementValidationLink());        
        // Access a dressed member    
	nClusOnTrack++;
	
	float globalX= sctCluster->globalX();
	float globalY= sctCluster->globalY();
	float globalZ= sctCluster->globalZ();
	float globalR=pow(globalX*globalX+globalY*globalY,0.5);
	if (globalY<0) globalR=-1.*globalR;
	m_sctClusterGlobalRZTrack->Fill(globalZ, globalR);
	if (fabs(globalZ)<500.)
	  m_sctClusterGlobalXYTrack->Fill(globalX, globalY);

	m_sctClusOnTrackLocalX->Fill(sctCluster->localX());
	
	///float localPhi = sctCluster->auxdata<float>("localPhi");
	float localPhi = msos->localPhi();
	float localTheta = msos->localTheta();
	m_sctClusLocalPhi->Fill(localPhi);
	m_sctClusLocalTheta->Fill(localTheta);
	if ( (sctCluster)->isAvailable< int  >( "SiWidth" ) ) {
	  m_sctClusSizeVsLocalPhi->Fill(localPhi,(sctCluster)->auxdataConst<int>("SiWidth"));
	  m_sctClusOnTrackSize->Fill((sctCluster)->auxdataConst<int>("SiWidth"));
	} else {
	  std::cout<<" SiWidth variable not available"<<std::endl;
	  
	}
	if (   m_doTimeBin &&  (sctCluster)->isAvailable< std::vector<int>  >( "rdo_timebin" ) ) {
	  std::vector<int> rdoTimebin = (sctCluster)->auxdataConst< std::vector<int>  >( "rdo_timebin" );
	  std::vector<int>::iterator rdoIter=rdoTimebin.begin();
	  for (; rdoIter!=rdoTimebin.end(); ++rdoIter) {
	    m_sctClusOnTrackTimeBin->Fill((*rdoIter));
	    // std::cout<<" timebin is "<<(*rdoIter)<<std::endl;
	  }
	}
	
	m_sctClusUnbiasedResidualX->Fill(msos->unbiasedResidualX());
	

	int bec = (sctCluster)->auxdataConst<int>("bec");
	int layer = (sctCluster)->auxdataConst<int>("layer");
	int side = (sctCluster)->auxdataConst<int>("side");
	int eta_module = (sctCluster)->auxdataConst<int>("eta_module");
	int phi_module = (sctCluster)->auxdataConst<int>("phi_module");

	if ((bec==0) && (layer < 4)) {
	  int index = side*4+layer;
	  m_sctClusterOnTrackMapBarrel[index]->Fill(eta_module,phi_module);
	} else if ((bec==-2) && (layer < 9)) {
	  int index = side*9+layer;
	  m_sctClusterOnTrackMapECC[index]->Fill(eta_module,phi_module);
	} else if ((bec==2) && (layer < 9)) {
	  int index = side*9+layer;
	  m_sctClusterOnTrackMapECA[index]->Fill(eta_module,phi_module);
	}
	
      }
      
    }
    ///    std::cout<<" Number of SCT clusters "<<nClusOnTrack<<" "<<nSCTHits<<std::endl;
    
	    
  }
  
}

void 
SCTClusterOnTrackPlots::finalize() 
{
  
}



