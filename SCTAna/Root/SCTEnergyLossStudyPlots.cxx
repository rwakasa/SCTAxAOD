#include <iostream>
#include "SCTAna/SCTEnergyLossStudyPlots.h"


// EDM includes: - if move to header file will not compile?
#include "xAODEventInfo/EventInfo.h"
#include "xAODTruth/TruthParticleAuxContainer.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthVertex.h"
#include "xAODTracking/TrackMeasurementValidationContainer.h"
#include "xAODTracking/TrackStateValidationContainer.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTracking/TrackParticleAuxContainer.h"

#include "xAODRootAccess/TStore.h"
#include "xAODCore/ShallowCopy.h"

SCTEnergyLossStudyPlots::SCTEnergyLossStudyPlots() 
{
  std::cout<<"in SCTEnergyLossStudyPlots constructor"<<std::endl;
  m_histList = new TList();
}

void SCTEnergyLossStudyPlots::bookHists() 
{
  std::vector<TString> binNames;
  binNames.push_back("000");
  binNames.push_back("001");
  binNames.push_back("010");
  binNames.push_back("011");
  binNames.push_back("100");
  binNames.push_back("101");
  binNames.push_back("110");
  binNames.push_back("111");
  
  m_timeBinTrack = new TH1F("timeBinTrack","timeBinTrack",binNames.size(),-0.5,binNames.size()-0.5);

  for (unsigned int i=0; i< binNames.size(); ++i) {
    m_timeBinTrack->GetXaxis()->SetBinLabel(i+1, binNames[i]);

  }

  m_timeBinHist= new TH1F("TimeBinHist","3-bin",3,0,3);//For mean of the entire 3-timebin distribution
  m_timeBinProfile = new TProfile("TimeBinProfile","layers; mean timebin; mean timebin across all layers",50,0.,22.);

  m_sctClusLocalPhi_before = new TH1F("sctClusLocalPhi_before","; local phi", 100,-1.,1.); 
  m_sctClusLocalTheta_before = new TH1F("sctClusLocalTheta_before","; local theta", 100,-3.,3.); 
  m_sctClusLocalPhi_after = new TH1F("sctClusLocalPhi_after","; local phi", 100,-1.,1.); 
  m_sctClusLocalTheta_after = new TH1F("sctClusLocalTheta_after","; local theta", 100,-3.,3.); 

  m_dEdxVsqp =  new TH2F("dEdxVsqp","; qp [MeV];dE/dx", 100, -5000., 5000., 100, 0, 6);
  m_dEdxSCT = new TH1F("dEdx_SCT"," dE/dx", 100,0.,6.); 
  
  m_hitsOntrack= new TH1F("hitsOntrack","hitsOntrack",100,0,20);

  m_histList->Add(m_timeBinTrack);
  m_histList->Add(m_timeBinHist);
  m_histList->Add(m_timeBinProfile);
  m_histList->Add(m_sctClusLocalPhi_before);
  m_histList->Add(m_sctClusLocalTheta_before);
  m_histList->Add(m_sctClusLocalPhi_after);
  m_histList->Add(m_sctClusLocalTheta_after);
  m_histList->Add(m_dEdxSCT);
  m_histList->Add(m_dEdxVsqp);
  m_histList->Add(m_hitsOntrack);
}

TList* 
SCTEnergyLossStudyPlots::getHists() 
{
  return m_histList;
}


void 
SCTEnergyLossStudyPlots::execute(xAOD::TEvent* thisEvent) 
{
  //Reset to not overcount first cluster
  m_timeBinHist->Reset();

  // get track container of interest
  const xAOD::TrackParticleContainer* recoTracks = 0;
  if ( !thisEvent->retrieve( recoTracks, "InDetTrackParticles" ).isSuccess() ){ // retrieve arguments: container type, container key
    Error("execute()", "Failed to retrieve Reconstructed Track container. Exiting." );
    return;//// EL::StatusCode::FAILURE;
  }


  // loop over the tracks in the container
  for( xAOD::TrackParticleContainer::const_iterator recoTrk_itr = recoTracks->begin(); recoTrk_itr != recoTracks->end(); recoTrk_itr++) {
    
    // Make track selection
    if ( (*recoTrk_itr)->pt() < 200 )
      continue;

    //Info("execute()",  "Track  (pT, eta, phi)  = (%f, %f, %f)", (*recoTrk_itr)->pt(), (*recoTrk_itr)->eta(), (*recoTrk_itr)->phi() );
    

    typedef std::vector<ElementLink< xAOD::TrackStateValidationContainer > > MeasurementsOnTrack;
    typedef std::vector<ElementLink< xAOD::TrackStateValidationContainer > >::const_iterator MeasurementsOnTrackIter;

    static const char* measurementNames = "msosLink";   //Note the prefix could change

    // Check if there are MSOS attached
    if( ! (*recoTrk_itr)->isAvailable< MeasurementsOnTrack >( measurementNames ) ) {
      std::cout<<" Measurements on track not available"<<std::endl;
      continue;
    }
  
    //Per-track relevant quantities
    int nClusterOnTrack=0;
    float dEdx=0.0;

    // Get the MSOS's
    const MeasurementsOnTrack& measurementsOnTrack = (*recoTrk_itr)->auxdata< MeasurementsOnTrack >( measurementNames );

    //Loop over track TrackStateValidation's (clusters)
    for( MeasurementsOnTrackIter msos_iter = measurementsOnTrack.begin(); msos_iter != measurementsOnTrack.end(); ++msos_iter) {  

      //Check if the element link is valid
      if( ! (*msos_iter).isValid() ) {
        continue;
      }
     
      const xAOD::TrackStateValidation* msos = *(*msos_iter); 
      if( (int)(msos->detType()) != 2 ) continue; /// 1 is pixel, 2 is SCT
      
      //Get SCT clusters
      if(msos->trackMeasurementValidationLink().isValid() && *(msos->trackMeasurementValidationLink())){

        const xAOD::TrackMeasurementValidation* sctCluster =  *(msos->trackMeasurementValidationLink());        
        // Access a dressed member    
        
        nClusterOnTrack++;
        //std::cout<<" I am in cluster-on-track "<<nClusterOnTrack<<std::endl;

        //there is one timebin int for each strip in the cluster, so timebin.size() is the number of strips in the cluster
        std::vector<int> timebin=(sctCluster)->auxdataConst< std::vector<int>  >( "rdo_timebin" );

        int layer = (sctCluster)->auxdataConst<int>("layer");
        float localPhi = msos->localPhi();
        float localTheta = msos->localTheta();
       
        m_sctClusLocalPhi_before->Fill(localPhi);
        m_sctClusLocalTheta_before->Fill(localTheta);

        //Redifining angles between -Pi/2 and Pi/2
        if (localPhi > TMath::Pi()/2.)
          localPhi -= TMath::Pi();
        if (localPhi < -TMath::Pi()/2.)
          localPhi += TMath::Pi();

        if (localTheta > TMath::Pi()/2.)
          localTheta -= TMath::Pi();
        if (localTheta < -TMath::Pi()/2.)
          localTheta += TMath::Pi();

        m_sctClusLocalPhi_after->Fill(localPhi);
        m_sctClusLocalTheta_after->Fill(localTheta);

        float alpha = atan(sqrt(pow(tan(localTheta), 2) + pow(tan(localPhi), 2)));
        float cos_alpha = cos(alpha);
        
        float clusterdEdx=0.0;
        float weight=0.0;

        for(unsigned int strip=0; strip<timebin.size();strip++){
          m_timeBinTrack->Fill(timebin[strip]);
          
          //std::cout<<"time bin in strip "<<strip<<" is "<<timebin[strip]<<std::endl;
          //std::cout<<"total number of strips in cluster-on-track is  "<<timebin.size()<<std::endl;

          // look over events->tracks->clusters->strips
          for (int j =0;j<4;j++){
            if (timebin[strip] >> j & 1){   //is the i-th bit a 1?
              m_timeBinHist->Fill(2-j);
            }
          }

          if (timebin[strip]==0 || timebin[strip]==5)
            weight=0.0;
          else if (timebin[strip]==1 || timebin[strip]==2 || timebin[strip]==4)
            weight=1.0;  
          else if (timebin[strip]==3 || timebin[strip]==6)
            weight=2.0;
          else if (timebin[strip]==7)
            weight=3.0;

        //std::cout<<"timebin[strip] ="<<timebin[strip]<<std::endl;
        //std::cout<<"weight is ="<<weight<<std::endl;
        //If there is more strips it looses more energy, higher weights
          clusterdEdx += weight;

        }//close strip loop
      
        float mean=m_timeBinHist->GetMean();
        //std::cout<<" Mean is "<<mean<<std::endl;
        //This does not give me all 22 layers, need to combine with "bec" variable, also need to reset and save one mean for each layer
        //then fill Profile at the very end
        m_timeBinProfile->Fill(layer,mean);
        
        //Now calculate the track's dE/dx for each cluster-on-track (hit)
        dEdx += (clusterdEdx * cos_alpha);
       
        //std::cout<<"clusterdEdx is = "<< clusterdEdx <<std::endl;
        //std::cout<<"cos_alpha      = "<<cos_alpha << std::endl;

      }//Close Get SCT Cluster
    }//Close MSOS

    ////////////
    //SCT dE/dx 
    ///////////
    //need to normalize by the clusters on the track we are looking at (or number of hits on the track)
    dEdx = dEdx / nClusterOnTrack;

    float qp = 1.0/(*recoTrk_itr)->qOverP();

    //if (dEdx>0. && nClusterOnTrack!=0){ 

    m_dEdxSCT->Fill(dEdx);
    m_dEdxVsqp->Fill(qp, dEdx);  

    //std::cout<<"dEdx is "<<dEdx<<std::endl;
    //std::cout<< "nClusterOnTrack is "<<nClusterOnTrack<<std::endl;

    //}
 
    m_hitsOntrack->Fill(nClusterOnTrack);

  }//Close tracks
}

void 
SCTEnergyLossStudyPlots::finalize() 
{
  
}



