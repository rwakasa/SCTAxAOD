#include <iostream>
#include "SCTAna/SCTOccupancyPlots.h"

#include "xAODEventInfo/EventInfo.h"
#include "xAODTracking/TrackMeasurementValidationContainer.h"
#include "xAODTracking/TrackStateValidationContainer.h"

#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/VertexAuxContainer.h"

#include "xAODRootAccess/TStore.h"
#include "xAODCore/ShallowCopy.h"

SCTOccupancyPlots::SCTOccupancyPlots() 
{
  std::cout<<"in SCTOccupancyPlots constructor"<<std::endl;
  m_histList = new TList();
  m_doTimeBin = true;
  std::string RootCoreBinDir = std::getenv("ROOTCOREBIN");

  m_isFirstEvent=true;
  
}
void SCTOccupancyPlots::bookHists() 
{

  m_eventCounter = new TH1F("eventCounter","; dummy",1,0,1);
  m_histList->Add(m_eventCounter);
  
  m_occupancyVsLayer = new TProfile("occupancyVsLayer","; ; Occupancy",22,0.,22.);
  std::vector<TString> binLabels;
  binLabels.push_back("ECC Disk 9");
  binLabels.push_back("ECC Disk 8");
  binLabels.push_back("ECC Disk 7");
  binLabels.push_back("ECC Disk 6");
  binLabels.push_back("ECC Disk 5");
  binLabels.push_back("ECC Disk 4");
  binLabels.push_back("ECC Disk 3");
  binLabels.push_back("ECC Disk 2");
  binLabels.push_back("ECC Disk 1");
  binLabels.push_back("Barrel 3");
  binLabels.push_back("Barrel 4");
  binLabels.push_back("Barrel 5");
  binLabels.push_back("Barrel 6");
  binLabels.push_back("ECA Disk 1");
  binLabels.push_back("ECA Disk 2");
  binLabels.push_back("ECA Disk 3");
  binLabels.push_back("ECA Disk 4");
  binLabels.push_back("ECA Disk 5");
  binLabels.push_back("ECA Disk 6");
  binLabels.push_back("ECA Disk 7");
  binLabels.push_back("ECA Disk 8");
  binLabels.push_back("ECA Disk 9");
  for (int i=0; i<22; ++i) m_occupancyVsLayer->GetXaxis()->SetBinLabel(i+1,binLabels[i]);
  

  m_histList->Add(m_occupancyVsLayer);
  
  m_occupancyVsMu_barrel0 = new TProfile("occupancyVsMu_barrel0",";#mu",25,0.,25.);
  m_histList->Add(m_occupancyVsMu_barrel0);
  m_occupancyVsMu_barrel1 = new TProfile("occupancyVsMu_barrel1",";#mu",25,0.,25.);
  m_histList->Add(m_occupancyVsMu_barrel1);
  m_occupancyVsMu_barrel2 = new TProfile("occupancyVsMu_barrel2",";#mu",25,0.,25.);
  m_histList->Add(m_occupancyVsMu_barrel2);
  m_occupancyVsMu_barrel3 = new TProfile("occupancyVsMu_barrel3",";#mu",25,0.,25.);
  m_histList->Add(m_occupancyVsMu_barrel3);
  
  

  TString histName="";
  for (int j=0; j<2;++j) {
    for (int i=0; i<4; ++i) {
      histName="sctOccupancyMapBarrel_";
      histName+=i;
      histName+="_";
      histName+=j;
      int index=4*j+i;
      m_sctOccupancyMapBarrel[index] = new TH2F(histName,"barrel",13,-6.5,6.5,60,-0.5,59.5);
      m_histList->Add(m_sctOccupancyMapBarrel[index]);
      
    }
    for (int i=0; i<9; ++i) {
      histName="sctOccupancyMapECA_";
      histName+=i;
      histName+="_";
      histName+=j;
      int index=9*j+i;
      m_sctOccupancyMapECA[index] = new TH2F(histName,"ECA",3,-0.5,2.5,60,-0.5,59.5);
      m_histList->Add(m_sctOccupancyMapECA[index]);
      histName="sctOccupancyMapECC_";
      histName+=i;
      histName+="_";
      histName+=j;
      m_sctOccupancyMapECC[index] = new TH2F(histName,"ECC",3,-0.5,2.5,60,-0.5,59.5);
      m_histList->Add(m_sctOccupancyMapECC[index]);
    }
  }
  ///  going from outer disk of ECC to outermost disk of ECA
  m_numModulesPerLayer = new std::vector<int>;
  m_numModulesPerLayer->push_back(39);  /// 13 modules disabled
  m_numModulesPerLayer->push_back(92);  ///Disk 8
  m_numModulesPerLayer->push_back(92);  ///Disk 7 
  m_numModulesPerLayer->push_back(132);  ///Disk 6 
  m_numModulesPerLayer->push_back(132);  ///Disk 5 
  m_numModulesPerLayer->push_back(132);  ///Disk 4 
  m_numModulesPerLayer->push_back(132);  ///Disk 3 
  m_numModulesPerLayer->push_back(132);  ///Disk 2 
  m_numModulesPerLayer->push_back(92);  ///Disk 1 
  m_numModulesPerLayer->push_back(384);  ///Barrel 0    
  m_numModulesPerLayer->push_back(480);  ///Barrel 1    
  m_numModulesPerLayer->push_back(576);  ///Barrel 2    
  m_numModulesPerLayer->push_back(672);  ///Barrel 3
  m_numModulesPerLayer->push_back(92);  ///Disk 1 
  m_numModulesPerLayer->push_back(132);  ///Disk 2
  m_numModulesPerLayer->push_back(132);  ///Disk 3 
  m_numModulesPerLayer->push_back(132);  ///Disk 4 
  m_numModulesPerLayer->push_back(132);  ///Disk 5 
  m_numModulesPerLayer->push_back(132);  ///Disk 6
  m_numModulesPerLayer->push_back(92);  ///Disk 7 
  m_numModulesPerLayer->push_back(92);  ///Disk 8 
  m_numModulesPerLayer->push_back(52);  ///Disk 9
    
}

TList* 
SCTOccupancyPlots::getHists() 
{
  return m_histList;
}

void 
SCTOccupancyPlots::execute(xAOD::TEvent* thisEvent) 
{

  //----------------------------
  // Event information
  //--------------------------- 

  
  const xAOD::EventInfo* eventInfo = 0;
  if( ! thisEvent->retrieve( eventInfo, "EventInfo").isSuccess() ){
    std::cout<<"Failed to retrieve eventInfo"<<std::endl;
    return;
  }
  int lumiBlock = eventInfo->lumiBlock();
  int runNumber = eventInfo->runNumber();
  float mu = eventInfo->auxdata<float>("actualInteractionsPerCrossing");
  //  std::cout<<" lumi block and mu are "<<lumiBlock<<" "<<mu<<std::endl;
  
  //  std::cout<<"actualInteractionsPerCrossing "<<mu<<" "<<eventInfo->actualInteractionsPerCrossing()<<std::endl;
  

  if (runNumber==276073 && ((lumiBlock < 465) || (lumiBlock > 622) ) ) return;  /// ONLY FOR RUN 276073!!!
    

  m_eventCounter->Fill(0.5);

  /**  trigger stuff - relies on TrigMenu object being there.
// Initialize and configure trigger tools on the first event
  if (m_isFirstEvent) {
    
    m_trigConfigTool = new TrigConf::xAODConfigTool("xAODConfigTool"); // gives us access to the meta-data
    StatusCode sc = m_trigConfigTool->initialize() ;
    if (sc.isFailure()) std::cout<<" trigConfigTool::initialize failed."<<std::endl;
    
    ToolHandle< TrigConf::ITrigConfigTool > trigConfigHandle( m_trigConfigTool );
    m_trigDecisionTool = new Trig::TrigDecisionTool("TrigDecisionTool");
    sc = m_trigDecisionTool->setProperty( "ConfigTool", trigConfigHandle ) ; // connect the TrigDecisionTool to the ConfigTool
    if (sc.isFailure()) std::cout<<" connecting TDT to ConfigTool failed."<<std::endl;
    sc = m_trigDecisionTool->setProperty( "TrigDecisionKey", "xTrigDecision" ) ;
    if (sc.isFailure()) std::cout<<" trigDecisionTool, finding key failed."<<std::endl;
    sc = m_trigDecisionTool->initialize() ;
    if (sc.isFailure()) std::cout<<" trigDecisionTool::initialize failed."<<std::endl;
    m_isFirstEvent=false;
    
  }



// examine the HLT_xe80* chains, see if they passed/failed and their total prescale
    auto chainGroup = m_trigDecisionTool->getChainGroup("L1_BCM_AC_CA_UNPAIRED_ISO.*");
    std::map<std::string,int> triggerCounts;
    for(auto &trig : chainGroup->getListOfTriggers()) {
      auto cg = m_trigDecisionTool->getChainGroup(trig);
      std::string thisTrig = trig;
      Info( "execute()", "%30s chain passed(1)/failed(0): %d total chain prescale (L1*HLT): %.1f", thisTrig.c_str(), cg->isPassed(), cg->getPrescale() );
    } // end for loop (c++11 style) over chain group matching "HLT_xe80*" 

  */  // end of trigger stuff
   

  std::vector<int> hitsPerLayer;
  for (int ib = 0; ib<22; ++ib) hitsPerLayer.push_back(0);



  const xAOD::TrackMeasurementValidationContainer* sctClusters = 0;
  if ( !thisEvent->retrieve( sctClusters, "SCT_Clusters" ).isSuccess() ){ // retrieve arguments: container type, container key
    Error("execute()", "Failed to retrieve SCT Clusters. Exiting." );
    return;//// EL::StatusCode::FAILURE;
  }
 
  ///  std::cout<<" number of SCT clusters is "<<sctClusters->size()<<std::endl;
  
 
  ///  Info("execute()",  "Looping over the PRD container");
  for( xAOD::TrackMeasurementValidationContainer::const_iterator cluster_itr = sctClusters->begin();
       cluster_itr != sctClusters->end(); cluster_itr++) 
  {  

    int bec = (*cluster_itr)->auxdataConst<int>("bec");
    int layer = (*cluster_itr)->auxdataConst<int>("layer");
    int side = (*cluster_itr)->auxdataConst<int>("side");
    int eta_module = (*cluster_itr)->auxdataConst<int>("eta_module");
    int phi_module = (*cluster_itr)->auxdataConst<int>("phi_module");

    /// find a layer bin index for the occupancy-vs-layer plot
    /// start from outermost layer of ECC as bin 0, up to outermost 
    /// layer of ECA as bin 21.


    int layerBinIndex = 0;
    if (bec == -2) layerBinIndex = 8 - layer;
    else if (bec== 0) layerBinIndex = 9+layer;
    else if (bec == 2) layerBinIndex = 13+layer;
    

    int weight=0;
    
    if ( m_doTimeBin && (*cluster_itr)->isAvailable< std::vector<int>  >( "rdo_timebin" ) ) {
	std::vector<int> rdoTimebin = (*cluster_itr)->auxdataConst< std::vector<int>  >( "rdo_timebin" );
	std::vector<int>::iterator rdoIter=rdoTimebin.begin();
	for (; rdoIter!=rdoTimebin.end(); ++rdoIter) {
	  for (int ibin = 0; ibin<3; ibin++) {
	    if (((*rdoIter) >> ibin) & 1) weight++;
	    
	  }
	}	
    } 
    ///    std::cout<<" weight is "<<weight<<std::endl;
    
    hitsPerLayer[layerBinIndex]+=weight;
    

    if ((bec==0) && (layer < 4)) {
	int index = 4*side+layer;
	m_sctOccupancyMapBarrel[index]->Fill(eta_module,phi_module,weight*0.3333/768.);	
    } else if ((bec==-2) && (layer < 9)) {
	int index = 9*side+layer;
	m_sctOccupancyMapECC[index]->Fill(eta_module,phi_module,weight*0.3333/768.);
    } else if ((bec==2) && (layer < 9)) {
	int index = 9*side+layer;
	m_sctOccupancyMapECA[index]->Fill(eta_module,phi_module,weight*0.3333/768.);
    }

  }  
  for (int ibin = 0; ibin<22; ibin++) 
    m_occupancyVsLayer->Fill(ibin+0.5, hitsPerLayer[ibin]*1.0/(768.0*2.0*m_numModulesPerLayer->at(ibin)*3.0));
  
  m_occupancyVsMu_barrel0->Fill(mu, hitsPerLayer[9]*1.0/(768.0*2.0*m_numModulesPerLayer->at(9)*3.0));
  m_occupancyVsMu_barrel1->Fill(mu, hitsPerLayer[10]*1.0/(768.0*2.0*m_numModulesPerLayer->at(10)*3.0));
  m_occupancyVsMu_barrel2->Fill(mu, hitsPerLayer[11]*1.0/(768.0*2.0*m_numModulesPerLayer->at(11)*3.0));
  m_occupancyVsMu_barrel3->Fill(mu, hitsPerLayer[12]*1.0/(768.0*2.0*m_numModulesPerLayer->at(12)*3.0));
  


}

void 
SCTOccupancyPlots::finalize() 
{

  float numEvents = m_eventCounter->GetEntries();
  for (int i=0; i<4; ++i) 
    m_sctOccupancyMapBarrel[i]->Scale(1.0/numEvents);
  for (int i=0; i<9; ++i) {
    m_sctOccupancyMapECA[i]->Scale(1.0/numEvents);
    m_sctOccupancyMapECC[i]->Scale(1.0/numEvents);
  }
  


  /**
  // cleaning up trigger tools
  if( m_trigConfigTool ) {
      delete m_trigConfigTool;
      m_trigConfigTool = 0;
   }
   if( m_trigDecisionTool ) {
      delete m_trigDecisionTool;
      m_trigDecisionTool = 0;
   }

  */

}



