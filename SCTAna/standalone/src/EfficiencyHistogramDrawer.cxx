#include <iostream>

#include "TROOT.h"
#include "TApplication.h"
#include "TFile.h"
#include "TTree.h"
#include "TH2.h"
#include "TProfile.h"
#include "TProfile2D.h"
#include "TCanvas.h"
#include "TString.h"
#include "TLeaf.h"
#include "TStyle.h"

int main(int argc, char *argv[]){

  if( argc != 6 ){
    std::cerr << "Usage: " << argv[0] << " [Ntuple file] [bec] [layer] [eta] [phi]" << std::endl;
    return -1;
  }

  /** Here user parameters **/

  // Histogram setting
  int nBinsX = 100;
  int nBinsY = 100;
  float rangeX[2] = {-65.0, 65.0}; // x range ([0] = min, [1] = max)
  float rangeY[2] = {-65.0, 65.0}; // y range ([0] = min, [1] = max)
  
  /**************************/

  TString fileName = argv[1];
  int bec   = atoi(argv[2]);
  int layer = atoi(argv[3]);
  int eta   = atoi(argv[4]);
  int phi   = atoi(argv[5]);

  if( bec != -2 && bec != 0 && bec != 2 ){
    std::cerr << "Error: bec must be -2, 0 or 2. Current value is " << bec << "." << std::endl;
    return -2;
  }

  // Protection for the module index. To be completed.
  if( bec == 0 ){
    if(abs(eta) < 1 || abs(eta) > 6){
      std::cerr << "Error: layer must be -6 - -1 or 1-6 for bec " << bec << ". Current value is " << eta << "." << std::endl;
      return -2;
    }
    if(layer == 0){
      if(phi < 0 || phi > 32){
	std::cerr << "Error: phi must be 0-32 for bec " << bec << ". Current value is " << eta << "." << std::endl;
	return -2;
      }
    }
    /*
      else {
      std::cerr << "Error: layer must be 0-4 for bec " << bec << ". Current value is " << layer << "." << std::endl;
      //return -2;
      }
    */
  }
  
  if( bec == 0 && (layer < 0 || layer > 3) ){
    //    std::cerr << "Error: layer must be 0-4 for bec " << bec << ". Current value is " << layer << "." << std::endl;
    std::cerr << "Error: layer must be 0-3 for bec " << bec << ". Current value is " << layer << "." << std::endl;
    return -2;
  }

  TApplication *app = new TApplication("app", &argc, argv);
  
  gROOT->ProcessLine("#include <vector>");  

  std::vector<int> *getHitBec   = 0;
  std::vector<int> *getHitLayer = 0;
  std::vector<int> *getHitEta   = 0;
  std::vector<int> *getHitPhi   = 0;
  std::vector<int> *getHitSide  = 0;
  std::vector<float> *getHitLocalX = 0;
  std::vector<float> *getHitLocalY = 0;
  std::vector<int> *getHoleBec   = 0;
  std::vector<int> *getHoleLayer = 0;
  std::vector<int> *getHoleEta   = 0;
  std::vector<int> *getHolePhi   = 0;
  std::vector<int> *getHoleSide  = 0;
  std::vector<float> *getHoleLocalX = 0;
  std::vector<float> *getHoleLocalY = 0;
  std::vector<int> *getHitStrip = 0;
  std::vector<int> *getHitClusize = 0;

  TFile *f = new TFile(fileName);
  if( f->IsZombie() ){
    std::cerr << "Error: " << fileName << " does not exist!" << std::endl;
    return -1;
  }
  TTree *t = (TTree*)f->Get("track");
  int nBranches = t->GetNbranches();

  /* Protection for contents of the branch; need to be added later.
  for( int i=0; i<nBranches; i++ ){
    std::cout << t->GetListOfBranches()->At(i)->GetName() << std::endl;
  }
  */


  t->SetBranchAddress("hit_bec",     &getHitBec);
  t->SetBranchAddress("hit_layer",   &getHitLayer);
  t->SetBranchAddress("hit_eta",     &getHitEta);
  t->SetBranchAddress("hit_phi",     &getHitPhi);
  t->SetBranchAddress("hit_side",    &getHitSide);
  t->SetBranchAddress("hit_localx",  &getHitLocalX);
  t->SetBranchAddress("hit_localy",  &getHitLocalY);
  t->SetBranchAddress("hit_strip",   &getHitStrip);
  t->SetBranchAddress("hit_clusize", &getHitClusize);
  t->SetBranchAddress("hole_bec",     &getHoleBec);
  t->SetBranchAddress("hole_layer",   &getHoleLayer);
  t->SetBranchAddress("hole_eta",     &getHoleEta);
  t->SetBranchAddress("hole_phi",     &getHolePhi);
  t->SetBranchAddress("hole_side",    &getHoleSide);
  t->SetBranchAddress("hole_localx",  &getHoleLocalX);
  t->SetBranchAddress("hole_localy",  &getHoleLocalY);


  int nEvents = t->GetEntries();

  TProfile *profEffAll = new TProfile("profAll", "profAll", 2, -0.5, 1.5);

  TProfile2D *profEff[2];
  profEff[0] = new TProfile2D("profEff0", TString::Format("Efficiency map (bec = %d, layer = %d, eta = %d, phi = %d): side 0;x (mm);y (mm)", bec, layer, eta, phi), nBinsX, rangeX[0], rangeX[1], nBinsY, rangeY[0], rangeY[1]);
  profEff[1] = new TProfile2D("profEff1", "side 1;x (mm);y (mm)", nBinsX, rangeX[0], rangeX[1], nBinsY, rangeY[0], rangeY[1]);
  TH1F *hist = new TH1F("hist", ";strip;", 768*2, 0, 768*2);

  for( int i=0; i<nEvents; i++ ){
    t->GetEntry(i);

    int nHitElements = getHitBec->size();
    int nHoleElements = getHoleBec->size();

    for( int j=0; j<nHitElements; j++ ){ // Hit loop
      if( getHitBec  ->at(j) == bec   &&
	  getHitLayer->at(j) == layer &&
	  getHitEta  ->at(j) == eta   &&
	  getHitPhi  ->at(j) == phi ){
	
	if( getHitSide->at(j) == 0 ){
	  profEff[0]->Fill( getHitLocalX->at(j), getHitLocalY->at(j), 1 );
	  profEffAll->Fill( 0.0, 1.0 );
	}else{
	  profEff[1]->Fill( getHitLocalX->at(j), getHitLocalY->at(j), 1 );
	  profEffAll->Fill( 1.0, 1.0 );
	}
	for( int strip=getHitStrip->at(j); strip<getHitStrip->at(j) + getHitClusize->at(j); strip++ ){
	  hist->Fill( getHitSide->at(j)*768 + strip );
	}
      }
    }
    for( int j=0; j<nHoleElements; j++ ){ // Hole loop
      if( getHoleBec  ->at(j) == bec   &&
	  getHoleLayer->at(j) == layer &&
	  getHoleEta  ->at(j) == eta   &&
	  getHolePhi  ->at(j) == phi ){

	if( getHoleSide->at(j) == 0 ){
	  profEff[0]->Fill( getHoleLocalX->at(j), getHoleLocalY->at(j), 0 );
	  profEffAll->Fill( 0.0, 0.0 );
	}else{
	  profEff[1]->Fill( getHoleLocalX->at(j), getHoleLocalY->at(j), 0 );
	  profEffAll->Fill( 1.0, 0.0 );
	}
      }
    }
    if( i % 10000 == 9999 ){
      std::cout << i+1 << " events have been done out of " << nEvents << " events." << std::endl;
    }
  }

  TCanvas *c1 = new TCanvas("c1", "c1", 1400, 700);
  gStyle->SetOptStat(0);

  c1->Divide(2, 1);

  c1->cd(1);
  profEff[0]->Draw("colz");

  c1->cd(2);
  profEff[1]->Draw("colz");

  std::cout << "" << std::endl;
  std::cout << "Efficiency (side 0) = " << profEffAll->GetBinContent(1) << " +/- " << profEffAll->GetBinError(1) << std::endl;
  std::cout << "Efficiency (side 1) = " << profEffAll->GetBinContent(2) << " +/- " << profEffAll->GetBinError(2) << std::endl;
  std::cout << "" << std::endl;
  std::cout << "Strips with no hit:" << std::endl;
  for( int i=0; i<768*2; i++ ){
    int val = hist->GetBinContent(i+1);
    if( val == 0 )std::cout << i << " ";
  } 
  std::cout << std::endl;

  TCanvas *c2 = new TCanvas("c2", "c2", 1400, 500);
  hist->Draw();
  app->Run();

  return 0;
}
