#include <iostream>
#include <vector>
#include "TROOT.h"
#include "TApplication.h"
#include "TFile.h"
#include "TTree.h"
#include "TH2.h"
#include "TProfile.h"
#include "TProfile2D.h"
#include "TCanvas.h"
#include "TString.h"
#include "TLeaf.h"
#include "TStyle.h"
#include "TString.h"

int main(int argc, char *argv[]){

  if( argc != 3 && argc != 4 ){
    std::cerr << "Usage: " << argv[0] << " [input file] [error type] [LB](optional)" << std::endl;
    std::cerr << "  type =  0: ByteStreamParseError" << std::endl;
    std::cerr << "  type =  1: TimeOutError" << std::endl;
    std::cerr << "  type =  2: BCIDError" << std::endl;
    std::cerr << "  type =  3: LVL1IDError" << std::endl;
    std::cerr << "  type =  4: PreambleError" << std::endl;
    std::cerr << "  type =  5: FormatterError" << std::endl;
    std::cerr << "  type =  6: TrailerError" << std::endl;
    std::cerr << "  type =  7: TrailerOverflowError" << std::endl;
    std::cerr << "  type =  8: HeaderTrailerError" << std::endl;
    std::cerr << "  type =  9: ABCDError" << std::endl;
    std::cerr << "  type = 10: RawError" << std::endl;
    std::cerr << "  type = 11: MaskedLink" << std::endl;
    std::cerr << "  type = 12: RODClockError" << std::endl;
    std::cerr << "  type = 13: TruncatedROD" << std::endl;
    std::cerr << "  type = 14: ROBFragmentError" << std::endl;
    std::cerr << "  type = 15: MissingLinkHeaderError" << std::endl;
    std::cerr << "  type = 16: MaskedROD" << std::endl;
    return -1;
  }

  TFile *f = new TFile(argv[1]);
  if( f->IsZombie() ){
    std::cerr << "Error: " << argv[1] << " does not exist!" << std::endl;
    return -1;
  }

  int targetType = atoi(argv[2]);
  int targetLB = -1;
  if( argc == 4 ){
    targetLB = atoi(argv[3]);
  }

  TTree *t = (TTree*)f->Get("event");

  int getLB = 0;
  std::vector<int> *getBsErrBec   = 0;
  std::vector<int> *getBsErrLayer = 0;
  std::vector<int> *getBsErrEta   = 0;
  std::vector<int> *getBsErrPhi   = 0;
  std::vector<int> *getBsErrSide  = 0;
  std::vector<int> *getBsErrType  = 0;

  
  t->SetBranchAddress("lb",            &getLB);
  t->SetBranchAddress("bsErr_bec",     &getBsErrBec);
  t->SetBranchAddress("bsErr_layer",   &getBsErrLayer);
  t->SetBranchAddress("bsErr_eta",     &getBsErrEta);
  t->SetBranchAddress("bsErr_phi",     &getBsErrPhi);
  t->SetBranchAddress("bsErr_side",    &getBsErrSide);
  t->SetBranchAddress("bsErr_type",    &getBsErrType);

  int nEvents = t->GetEntries();
  
  std::vector<TString> moduleList; 
  
  for( int i=0; i<nEvents; i++ ){
    t->GetEntry(i);
    if( targetLB != -1 && targetLB != getLB ) continue;
    
    int nSize = getBsErrBec->size();
    
    for( int j=0; j<nSize; j++ ){
      if( getBsErrType->at(j) == targetType ){
	moduleList.push_back( TString::Format("%02d%d%02d%02d%d", getBsErrBec->at(j), getBsErrLayer->at(j), getBsErrEta->at(j), getBsErrPhi->at(j), getBsErrSide->at(j)) );
      }
    }
    std::sort(moduleList.begin(), moduleList.end());
    moduleList.erase(std::unique(moduleList.begin(), moduleList.end()), moduleList.end());
    std::cout << "======================" << std::endl;
    for( int j=0; j<moduleList.size(); j++ ){
      std::cout << moduleList.at(j) << std::endl;
    }
    std::cout << "In total " << moduleList.size() << " links issued errors." << std::endl;
  }
}
