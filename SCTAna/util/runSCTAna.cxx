

#include "xAODRootAccess/Init.h"
#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/ToolsDiscovery.h"
#include "EventLoop/Job.h"
#include "EventLoop/DirectDriver.h"


#include "SCTAna/SCTEventLoop.h"

int main( int argc, char* argv[] ) {

  if( argc != 1 && argc != 2 ){
    std::cerr << "Usage: runSCTAna [input file list](optional)" << std::endl;
    return -1;
  }

  std::string inputfiles = "inputfiles.txt";
  if( argc == 2 ){
    inputfiles = argv[1];
  }
  
  // Take the submit directory from the input if provided:
  std::string submitDir = "submitDir";
  //if( argc > 1 ) submitDir = argv[ 1 ];

  // Set up the job for xAOD access:
  xAOD::Init().ignore();

  // Construct the samples to run on:
  SH::SampleHandler sh;
  ////  SH::scanDir( sh, "/r03/atlas/nbarlow/xAOD/TestInDetDxAOD");
  SH::readFileList (sh, "sample", inputfiles);
  // Set the name of the input TTree. It's always "CollectionTree"
  // for xAOD files.
  sh.setMetaString( "nc_tree", "CollectionTree" );

  // Print what we found:
  sh.print();

  // Create an EventLoop job:
  EL::Job job;
  job.sampleHandler( sh );

  // Add our analysis to the job:

  
  SCTEventLoop* alg = new SCTEventLoop();
  job.algsAdd( alg );


  

  // Run the job using the local/direct driver:
  EL::DirectDriver driver;

  
  driver.submit( job, submitDir );

  return 0;
}
