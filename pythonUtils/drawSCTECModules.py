
import ROOT,math
ROOT.gROOT.SetStyle("Plain")
ROOT.gStyle.SetPalette(1)
ROOT.gStyle.SetOptTitle(0)
ROOT.gStyle.SetOptStat(0)

### number of modules on inner, middle, outer rings for each disk
nmodules = {"1": {"2": 0,  "1": 40, "0": 52 },
            "2": {"2": 40, "1": 40, "0": 52 },
            "3": {"2": 40, "1": 40, "0": 52 },
            "4": {"2": 40, "1": 40, "0": 52 },
            "5": {"2": 40, "1": 40, "0": 52 },
            "6": {"2": 40, "1": 40, "0": 52 },
            "7": {"2": 0,  "1": 40, "0": 52 },
            "8": {"2": 0,  "1": 40, "0": 52 },
            "9": {"2": 0,  "1": 0,  "0": 52 }
            }

### outer radius of each disk (or inner radius of last disk).

radii = {"1": [560,440,320],
         "2": [560,440,334,275],
         "3": [560,440,334,275],
         "4": [560,440,334,275],
         "5": [560,440,334,275],
         "6": [560,440,334,275],
         "7": [560,440,320],
         "8": [560,455,402],
         "9": [560,440]         
         }

######### class ECDisk can take a TH2 of an SCT endcap disk, binned in
########   etaModule/phiModule coordinates, and return a pretty looking
########   picture of the endcap disk..
########   to use it, do something like
########   >>> import ROOT
########   >>> from drawSCTECModules import *
########   >>> myDisk = ECDisk(myHist, 3, 0.,5.)
########   >>> c1 = ROOT.TCanvas()
########   >>> myDisk.Draw()
#########  to draw myHist in this way, representing disk 3, with
#########  a z-axis scale going from 0 to 5.


class ECDisk:
    ### pretty much all the work is done in the constructor..
    def __init__(self,hist,diskNumber,zScaleMin=0.,zScaleMax=0.,isLogScale=False):

###   define the range on the z (colour scale) axis, if no
###   user-defined values are given, use the input hist.        
        if not (zScaleMin ==0 and zScaleMax ==0):
            self.zMin = zScaleMin
            self.zMax = zScaleMax
            pass
        else:
            self.zMin = hist.GetMinimum()
            self.zMax = hist.GetMaximum()

###  make a TH2F to get x and y axes, and fil it will a dummy value
        ### so we can get the z-scale (TPaletteAxis)
        self.bgHist = ROOT.TH2F("bg"+hist.GetName()+str(diskNumber),
                                "bg",10,-600,600,10,-600,600)
        self.bgHist.SetMinimum(self.zMin)
        self.bgHist.SetMaximum(self.zMax)
        self.bgHist.Fill(-1000,-1000,1.5)
        self.bgHist.GetXaxis().SetNdivisions(5)
        self.bgHist.GetYaxis().SetNdivisions(5)
        self.bgHist.SetXTitle("X (mm)")
        self.bgHist.SetYTitle("Y (mm)")
#### list of TPies, each of which represents inner, middle or outer rings.
        self.pies = []
        for i in range(radii[str(diskNumber)].__len__()-1):
            nmod = nmodules[str(diskNumber)][str(i)]
### dummy histogram h, filled with 1 entry per module,
### to be used to construct a TPie with nmod equal-sized slices.
            h = ROOT.TH1F("h"+str(i),"h",nmod,0,nmod)
            for j in range(nmod):
                h.Fill(j)
                pass
            pie = ROOT.TPie(h)
            pie.SetRadius(radii[str(diskNumber)][i])
            pie.SetLabelsOffset(10000) # move the labels off the plot
### now actually fill the slices of the TPie with the appropriate colour
            for j in range(nmod):
                sliceVal = hist.GetBinContent(i+1,j+1)
                sliceColour = self.getSliceColour(sliceVal,isLogScale)
                pie.SetEntryFillColor(j,sliceColour)
                pass
            self.pies += [pie]
            pass
# draw a white circle on the inside
        circrad = radii[str(diskNumber)][radii[str(diskNumber)].__len__()-1]
        self.circle = ROOT.TEllipse(0.,0.,circrad,circrad)
        self.circle.SetFillColor(0)
        pass


# utility function to translate a bin value into a colour.
# currently setup for using defaulr rainbow palette (gROOT.SetPalette(1)):
# i.e. the colours from 51=100
    def getSliceColour(self,val,isLogScale=False):
        if val == 0:
            return 0 # always return white for zeroes?    
        elif val >= self.zMax:
            return 100
        elif val <= self.zMin:
            return 51
        else:
            if not isLogScale:
                whereOnScale = float((val-self.zMin)/(self.zMax-self.zMin))
                col =int(whereOnScale*50.0) + 51
                return col
            else:
                if self.zMin==0.:
                    self.zMin=1e-5
                    pass
                whereOnScale = (math.log(val) - math.log(self.zMin))/(math.log(self.zMax)-math.log(self.zMin))
                col =int(whereOnScale*50.0) + 51
                return col

### draw the background hist, the pies, and the blank circle in the middle,
### in the right order..
    def Draw(self,withColAxis=True):
        if withColAxis:
            self.bgHist.Draw("colz")
        else:
            self.bgHist.Draw()
        for pie in self.pies:
            pie.Draw("same")
            pass
        self.circle.Draw("same")


####################################################################
#################### ok, that's the end of the class definition,###
##################### now, to test it...###########################

#testHist = ROOT.TH2F("testHist","testHist",3,0.,3,50,0.,50)
#
#for i in range(50):
#    for j in range(3):
#        testHist.Fill(j,i,0.5 + j*0.1 + i * 0.02)
#
#
#c1 = ROOT.TCanvas()
#disk = ECDisk(testHist,1)
#disk.Draw()



