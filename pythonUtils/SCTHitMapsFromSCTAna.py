
import ROOT,sys
from SCTPlottingUtils import *
##set_palette()
from drawSCTECModules import *


if "root" in sys.argv[-1]:
    filename=sys.argv[-1]
    pass
else:
    filename="histograms.root"

f=ROOT.TFile(filename)

h=GetFromFile(f)

barrelHists={}
ecaHists={}
eccHists={}
ecaPlots={}
eccPlots={}

hists=["sctClusterMap","sctClusterOnTrackMap","TimeOutErrorMap","BCIDErrorMap","LVL1IDErrorMap","TimeOutErrorMap","ROBFragmentErrorMap","MaskedLinkMap","RawErrorMap"]#,"FirstWordIsHitMap","StripsOutOfOrderMap","LinksOutOfOrderMap","EventIDErrorMap"]

for hist in hists:
    barrelHists[hist]=[]
    ecaHists[hist]=[]
    eccHists[hist]=[]
    ecaPlots[hist]=[]
    eccPlots[hist]=[]

    for i in range(4):
        for j in range(2):
            barrelHists[hist]+=[h[hist+"Barrel_"+str(i)+"_"+str(j)]]
            pass
        pass
    for i in range(9):
        for j in range(2):
            ecaHists[hist]+=[h[hist+"ECA_"+str(i)+"_"+str(j)]]
            eccHists[hist]+=[h[hist+"ECC_"+str(i)+"_"+str(j)]]
#            if j==0:
#                ecaPlots[hist]+=[ECDisk(ecaHists[hist][i*2],str(i+1))]
#                eccPlots[hist]+=[ECDisk(eccHists[hist][i*2],str(i+1))]
#                pass
            pass
        pass
    pass

c=ROOT.TCanvas("c","c",1500,500)
c.Divide(3,1)
p1=c.cd(1)
p1.Divide(3,3)
p2=c.cd(2)
p2.Divide(2,2)
p3=c.cd(3)
p3.Divide(3,3)

def plotMaps(hist,side=0):
    
    zmax=0.0
    for histo in barrelHists[hist]:
        if histo.GetMaximum() > zmax:
            zmax= histo.GetMaximum()
            pass
        pass
    for histo in ecaHists[hist]:
        if histo.GetMaximum() > zmax:
            zmax= histo.GetMaximum()
            pass
        pass
    for histo in eccHists[hist]:
        if histo.GetMaximum() > zmax:
            zmax= histo.GetMaximum()
            pass
        pass
##    zmax=zmax/10.
    for histo in barrelHists[hist]:
        histo.SetMaximum(zmax)
        pass
    for histo in ecaHists[hist]:
        histo.SetMaximum(zmax)
        pass
    for histo in eccHists[hist]:
        histo.SetMaximum(zmax)
        pass

    for i in range(9):
        if i<4:
            p2.cd(i+1)
            if side==0:
                barrelHists[hist][i*2].Draw("colz")
                pass
            else:
                barrelHists[hist][i*2+1].Draw("colz")
                pass
            pass
        p1.cd(i+1)
        if side==0:
            eccPlots[hist]+=[ECDisk(eccHists[hist][i*2],str(i+1))]
            pass
        else:
            eccPlots[hist]+=[ECDisk(eccHists[hist][i*2+1],str(i+1))]
            pass            
        eccPlots[hist][i].Draw()
        p3.cd(i+1)
        if side==0:
            ecaPlots[hist]+=[ECDisk(ecaHists[hist][i*2],str(i+1))]
            pass
        else:
            ecaPlots[hist]+=[ECDisk(ecaHists[hist][i*2+1],str(i+1))]
            pass
        ecaPlots[hist][i].Draw()




