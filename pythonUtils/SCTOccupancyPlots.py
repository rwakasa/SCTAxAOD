
import ROOT,sys
from SCTPlottingUtils import *
##from makeFinalPlots import *
##set_palette()
from drawSCTECModules import *


def removePaletteAxis(hist):
    pal=hist.GetListOfFunctions().FindObject("palette")
    pal.SetX1NDC(220)
    pal.SetX2NDC(220)
    pal.Draw()


def expandPaletteAxis(hist):
    pal=hist.GetListOfFunctions().FindObject("palette")
    pal.SetX1NDC(0.1)
    pal.SetX2NDC(0.9)
    pal.SetY1NDC(0.1)
    pal.SetY2NDC(0.9)
    pal.GetAxis().SetLabelSize(0.08)
    pal.GetAxis().SetLabelOffset(0.)
    pal.GetAxis().SetTickSize(0.07)
    pal.Draw()


if "root" in sys.argv[-1]:
    filename=sys.argv[-1]
    pass
else:
    filename="histograms.root"

f=ROOT.TFile(filename)

h=GetFromFile(f)

barrelHists={}
ecaHists={} 
eccHists={}
ecaPlots={}
eccPlots={}

hists=["sctOccupancyMap"]

zmin=1e-4
zmax=2e-2

for hist in hists:
    barrelHists[hist]=[]
    ecaHists[hist]=[]
    eccHists[hist]=[]
    ecaPlots[hist]=[]
    eccPlots[hist]=[]

    for i in range(4):
        for j in range(2):
            barrelHists[hist]+=[h[hist+"Barrel_"+str(i)+"_"+str(j)]]
            pass
        pass
    for i in range(9):
        for j in range(2):
            ecaHists[hist]+=[h[hist+"ECA_"+str(i)+"_"+str(j)]]
            eccHists[hist]+=[h[hist+"ECC_"+str(i)+"_"+str(j)]]
#            if j==0:
#                ecaPlots[hist]+=[ECDisk(ecaHists[hist][i*2],str(i+1))]
#                eccPlots[hist]+=[ECDisk(eccHists[hist][i*2],str(i+1))]
#                pass
            pass
        pass
    pass




c=ROOT.TCanvas("c","c",800,1000)
c.SetTopMargin(0.3)

p1=ROOT.TPad("p1","p1",0.05,0.41,0.49,0.8)
p1.Draw()
p2=ROOT.TPad("p2","p2",0.51,0.41,0.99,0.8)
p2.Draw()
p3=ROOT.TPad("p3","p3",0.05,0.05,0.49,0.39)
p3.Draw()
p4=ROOT.TPad("p4","p4",0.71,0.05,0.81,0.39)
p4.Draw()

#
#
#c.Divide(2,1)
#p1=c.cd(1)
p1.Divide(3,3)
#p2=c.cd(2)
p2.Divide(3,3)

p3.Divide(2,2)



hist="sctOccupancyMap"
pads=[]

for histo in ecaHists[hist]:
    histo.SetMaximum(zmax)
    histo.SetMinimum(zmin)
    pass
for histo in eccHists[hist]:
    histo.SetMaximum(zmax)
    histo.SetMinimum(zmin)
    pass

for histo in barrelHists[hist]:
    histo.SetMaximum(zmax)
    histo.SetMinimum(zmin)
    pass

padCounter=0
for i in range(9):

    pads+=[p1.cd(i+1)]
    pads[padCounter].SetLogz()
    eccPlots[hist]+=[ECDisk(eccHists[hist][i*2],str(i+1),zmin,zmax,True)] # True here means logz
    eccPlots[hist][i].Draw(False) # False here means no z-colour axis shown
    padCounter+=1
    pads+=[p2.cd(i+1)]
    pads[padCounter].SetLogz()
    ecaPlots[hist]+=[ECDisk(ecaHists[hist][i*2],str(i+1),zmin,zmax,True)] # True here means logz
    ecaPlots[hist][i].Draw(False) # False here means no z-colour axis shown
    padCounter+=1
    if i<4:
        pads+=[p3.cd(i+1)]
        pads[padCounter].SetLogz()
        barrelHists[hist][i*2].Draw("colz")
        pass
    pass

c.cd()
tex = ROOT.TLatex()
tex.SetNDC()

tex.SetTextSize(0.048)

tex.DrawLatex(0.1, 0.93, "#font[72]{ATLAS} Internal");

tex.SetTextFont(42)
#tex.DrawLatex(0.7,0.93,"Run 260272, event 6539")
##tex.DrawLatex(0.5,0.93,"Run 276073")
tex.DrawLatex(0.5,0.93,"Run 276790")

l1=ROOT.TLine(0.5,0.4,0.5,0.8)
l1.SetLineWidth(3)
l1.SetLineColor(2)
l1.SetLineStyle(2)
l1.Draw()

cside=ROOT.TPaveText(0.18,0.8,0.38,0.85)
cside.SetBorderSize(0)
cside.SetFillColor(0)
cside.SetTextColor(2)
cside.AddText("Endcap C")
cside.Draw()

aside=ROOT.TPaveText(0.66,0.8,0.86,0.85)
aside.SetBorderSize(0)
aside.SetFillColor(0)
aside.SetTextColor(2)
aside.AddText("Endcap A")
aside.Draw()

p4.cd()
p4.SetLogz()
dummyHist=ROOT.TH2F("dummyHist","",10,0,1,10,0,1)
dummyHist.SetMinimum(zmin)
dummyHist.SetMaximum(zmax)
dummyHist.Fill(2,2)
dummyHist.GetXaxis().SetLabelColor(0)
dummyHist.GetYaxis().SetLabelColor(0)
dummyHist.Draw("colz")

c.Update()
p1.Update()
p2.Update()
p3.Update()
p4.Update()

for i in range(0,8,2):
    removePaletteAxis(barrelHists[hist][i])
    pass

c.Update()

p3.Update()
p4.Update()
expandPaletteAxis(dummyHist)

p4.Update()


#### HACK!!!!!

c.cd()

tpmin=ROOT.TPaveText(0.82,0.067,0.87,0.108)
tpmin.SetFillColor(0)
tpmin.SetBorderSize(0)
tpmin.AddText(str(zmin))
tpmin.Draw()


tpmax=ROOT.TPaveText(0.82,0.336,0.87,0.374)
tpmax.SetFillColor(0)
tpmax.SetBorderSize(0)
tpmax.AddText(str(zmax))
tpmax.Draw()
