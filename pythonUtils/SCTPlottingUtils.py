import user,ROOT,math
try:
    import numpy
except(ImportError):
    pass
from array import array
ROOT.gROOT.SetStyle("Plain")
ROOT.gStyle.SetOptStat(0)
ROOT.gStyle.SetOptTitle(0)

def GetFromFile(file):
    dict={}
    for key in file.GetListOfKeys():
        dict[key.GetName()] = file.Get(key.GetName())
        pass
    return dict

def getLegend(x1=0.1,y1=0.1,x2=0.3,y2=0.3,bordersize=0,fillcolour=0):
    l=ROOT.TLegend(x1,y1,x2,y2)
    l.SetFillColor(fillcolour)
    l.SetBorderSize(bordersize)
    return l

def set_palette(name="palette", ncontours=999):
    """Set a color palette from a given RGB list
    stops, red, green and blue should all be lists of the same length
    see set_decent_colors for an example"""
    if name == "gray" or name == "grayscale":
        stops = [0.00, 0.34, 0.61, 0.84, 1.00]
        red   = [1.00, 0.84, 0.61, 0.34, 0.00]
        green = [1.00, 0.84, 0.61, 0.34, 0.00]
        blue  = [1.00, 0.84, 0.61, 0.34, 0.00]
    elif name == "nick":
        stops = [0.00, 0.25, 0.5, 0.75, 1.00]
        red   = [0.0, 0.0, 0.0, 0,5, 1.0]
        green = [0., 0.5, 1.00, 0.5, 0.00]
        blue  = [1.0, 0.5, 0.0, 0.00, 0.00]        
    else:
        stops = [0.00, 0.34, 0.61, 0.84, 1.00]
        red   = [0.00, 0.00, 0.87, 1.00, 0.51]
        green = [0.00, 0.81, 1.00, 0.20, 0.00]
        blue  = [0.51, 1.00, 0.12, 0.00, 0.00]
    s = numpy.array(stops)
    r = numpy.array(red)
    g = numpy.array(green)
    b = numpy.array(blue)    
    npoints = len(s)
    ROOT.TColor.CreateGradientColorTable(npoints, s, r, g, b, ncontours)
    ROOT.gStyle.SetNumberContours(ncontours)
