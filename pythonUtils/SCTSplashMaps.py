
import ROOT,sys
from SCTPlottingUtils import *
##from makeFinalPlots import *
##set_palette()
from drawSCTECModules import *


if "root" in sys.argv[-1]:
    filename=sys.argv[-1]
    pass
else:
    filename="histograms.root"

f=ROOT.TFile(filename)

h=GetFromFile(f)

barrelHists={}
ecaHists={} 
eccHists={}
ecaPlots={}
eccPlots={}

hists=["sctClusterMap"]

for hist in hists:
    barrelHists[hist]=[]
    ecaHists[hist]=[]
    eccHists[hist]=[]
    ecaPlots[hist]=[]
    eccPlots[hist]=[]

    for i in range(4):
        for j in range(2):
            barrelHists[hist]+=[h[hist+"Barrel_"+str(i)+"_"+str(j)]]
            pass
        pass
    for i in range(9):
        for j in range(2):
            ecaHists[hist]+=[h[hist+"ECA_"+str(i)+"_"+str(j)]]
            eccHists[hist]+=[h[hist+"ECC_"+str(i)+"_"+str(j)]]
#            if j==0:
#                ecaPlots[hist]+=[ECDisk(ecaHists[hist][i*2],str(i+1))]
#                eccPlots[hist]+=[ECDisk(eccHists[hist][i*2],str(i+1))]
#                pass
            pass
        pass
    pass

c=ROOT.TCanvas("c","c",1000,550)
c.SetTopMargin(0.3)

p1=ROOT.TPad("p1","p1",0.05,0.05,0.49,0.8)
p1.Draw()
p2=ROOT.TPad("p2","p2",0.51,0.05,0.99,0.8)
p2.Draw()
#
#
#c.Divide(2,1)
#p1=c.cd(1)
p1.Divide(3,3)
#p2=c.cd(2)
p2.Divide(3,3)



hist="sctClusterMap"
pads=[]

for histo in ecaHists[hist]:
    histo.Scale(1.0/768.)
    histo.SetMaximum(1.0)
    histo.SetMinimum(1e-2)
    pass
for histo in eccHists[hist]:
    histo.Scale(1.0/768.)
    histo.SetMinimum(1e-2)
    histo.SetMaximum(1.0)
    pass
padCounter=0
for i in range(9):
    pads+=[p1.cd(i+1)]
    pads[padCounter].SetLogz()
    eccPlots[hist]+=[ECDisk(eccHists[hist][i*2],str(i+1),1e-2,1.,True)]
    eccPlots[hist][i].Draw(False)
    padCounter+=1
    pads+=[p2.cd(i+1)]
    pads[padCounter].SetLogz()
    ecaPlots[hist]+=[ECDisk(ecaHists[hist][i*2],str(i+1),1e-2,1.,True)]
    ecaPlots[hist][i].Draw(False)
    padCounter+=1

c.cd()
tex = ROOT.TLatex()
tex.SetNDC()

tex.SetTextSize(0.048)

tex.DrawLatex(0.2, 0.93, "#font[72]{ATLAS} Internal");

tex.SetTextFont(42)
#tex.DrawLatex(0.7,0.93,"Run 260272, event 6539")
tex.DrawLatex(0.7,0.93,"Run xxxxxx, event yyyyyy")

l1=ROOT.TLine(0.5,0.,0.5,0.9)
l1.SetLineWidth(3)
l1.SetLineColor(2)
l1.SetLineStyle(2)
l1.Draw()

cside=ROOT.TPaveText(0.18,0.8,0.38,0.85)
cside.SetBorderSize(0)
cside.SetFillColor(0)
cside.SetTextColor(2)
cside.AddText("Endcap C")
cside.Draw()

aside=ROOT.TPaveText(0.66,0.8,0.86,0.85)
aside.SetBorderSize(0)
aside.SetFillColor(0)
aside.SetTextColor(2)
aside.AddText("Endcap A")
aside.Draw()


def removePaletteAxis(hist):
    pal=hist.GetListOfFunctions().FindObject("palette")
    pal.SetX1NDC(220)
    pal.SetX2NDC(220)
    pal.Draw()


