import ROOT, sys,os
from SCTPlottingUtils import *

if "root" in sys.argv[-1]:
    filename=sys.argv[-1]
    pass
else:
    filename="histograms.root"

f=ROOT.TFile(filename)

h=GetFromFile(f)

h["occupancyVsMu_barrel0"].SetMarkerStyle(20)
h["occupancyVsMu_barrel0"].SetLineWidth(2)
h["occupancyVsMu_barrel0"].SetLineColor(1)
h["occupancyVsMu_barrel0"].SetMarkerColor(1)

h["occupancyVsMu_barrel1"].SetMarkerStyle(21)
h["occupancyVsMu_barrel1"].SetLineWidth(2)
h["occupancyVsMu_barrel1"].SetLineColor(2)
h["occupancyVsMu_barrel1"].SetMarkerColor(2)

h["occupancyVsMu_barrel2"].SetMarkerStyle(22)
h["occupancyVsMu_barrel2"].SetLineWidth(2)
h["occupancyVsMu_barrel2"].SetLineColor(3)
h["occupancyVsMu_barrel2"].SetMarkerColor(3)

h["occupancyVsMu_barrel3"].SetMarkerStyle(23)
h["occupancyVsMu_barrel3"].SetLineWidth(2)
h["occupancyVsMu_barrel3"].SetLineColor(4)
h["occupancyVsMu_barrel3"].SetMarkerColor(4)

h["occupancyVsMu_barrel0"].Draw()
h["occupancyVsMu_barrel1"].Draw("same")
h["occupancyVsMu_barrel2"].Draw("same")
h["occupancyVsMu_barrel3"].Draw("same")


l=ROOT.TLegend(0.7,0.2,0.88,0.4)
l.SetFillColor(0)
l.SetBorderSize(0)
l.AddEntry(h["occupancyVsMu_barrel0"],"Barrel 3","PL")
l.AddEntry(h["occupancyVsMu_barrel1"],"Barrel 4","PL")
l.AddEntry(h["occupancyVsMu_barrel2"],"Barrel 5","PL")
l.AddEntry(h["occupancyVsMu_barrel3"],"Barrel 6","PL")
